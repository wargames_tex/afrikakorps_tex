#
# TODO:
#
# - Make a `setup` phase
# - Place units at their starting positions
# - Limit actions to specific phases
# - Automatic expend RPs
#
from wgexport import *

# --------------------------------------------------------------------
# Calculate Hex coordinates from string 
def convHex(loc):
    try:
        int(loc)
        return None
    except:
        pass
    if '+' in loc:
        return None

    from math import floor 
    ordz       = ord('Z')
    orda       = ord('A')
    dz         = ordz - orda + 1
    acol, arow = loc[0], loc[1:]
    row        = int(arow)
    col        = ord(acol[0])-orda
    off        = int(col//2)
    row        = row - off
    icol       = col % dz
    tcol       = chr(icol + orda)
    return f'{tcol}{row}'

# --------------------------------------------------------------------
# Our main patching function
def patch(build,data,vmod,verbose=False):
    from re  import sub
    from PIL import Image
    from io  import BytesIO
    from pathlib import Path

    Verbose().setVerbose(verbose)
    verb = VerboseGuard()

    game = build.getGame()
    factionColors = {'Allied': (0xbb,0xae,0x81),
                     'Axis':   (0x8c,0xa0,0x95) };
    boardColor    = rgb(0xde,0xcb,0xa2)
    oldSchool     = 'images/oldschool-icon.png' in vmod.getFileNames()
    # print(vmod.getFileNames())
    if oldSchool:
        print(f'Using NATO colors for old-school verison')
        factionColors = {'Allied': (128,224,255), # NATO friendly
                         'Axis':   (255,128,128) }
        boardColor    = rgb(0xd1,0x90,0x29)
        
        
    oldSchool     = 'OS' in vmod.fileName()
    trailColors   = {f: rgb(*(c-0x20 for c in cl))
                     for f,cl in factionColors.items()}
    
    hexes  = {
        'U1': '', 
        'W2': '', 
        # --- 3 ---
        'V2': '', 
        'W3': '', 
        'X3': '', 
        # --- 4 ---
        'V3': '', 
        'W4': '', 
        'X4': '', 
        # --- 5 ---
        'V4': '', 
        'W5': '', 
        'X5': '', 
        # --- 6 ---
        'W6': '', #	Agheila
        'X6': '', 
        # --- 7 ---
        'W7': '', 
        'X7': '', 
        # --- 8 ---
        'K2': '', 
        'M3': '', 
        'W8': '', 
        'X8': '', 
        # --- 9 ---
        'I2': '', 
        'J2': '', 
        'K3': '', 
        'L3': '', 
        'M4': '', 
        'N4': '', 
        'O5': '', 
        'W9': '', 
        'X9': '', 
        # --- 10 ---
        'G2': '', 
        'H2': 'fortress', #	Benghazi
        'I3': '', 
        'J3': '', 
        'K4': '', 
        'L4': '', 
        'M5': '', 
        'N5': '', 
        'O6': '', 
        'P6': '', 
        'Q7': '', 
        'U9': '', 
        'V9': '', 
        'W10': '', 
        'X10': '', 
        # --- 11 ---
        'F2': '', 
        'G3': '', 
        'H3': '', 
        'I4': 'escarpment', 
        'J4': 'escarpment', 
        'K5': 'escarpment', 
        'L5': '', 
        'M6': '', 
        'N6': '', 
        'O7': '', 
        'P7': '', 
        'Q8': '', 
        'R8': '', 
        'S9': '', 
        'T9': '', 
        'U10': '', 
        'V10': '', 
        'W11': '', 
        'X11': '', 
        # --- 12 ---
        'E3': '', 
        'F3': '', 
        'G4': 'escarpment', 
        'H4': 'escarpment', 
        'I5': '', 
        'J5': '', 
        'K6': '', 
        'L6': '', 
        'M7': '', 
        'N7': '', 
        'O8': '', 
        'P8': '', 
        'Q9': '', 
        'R9': '', 
        'S10': '', 
        'T10': '', 
        'U11': '', # Agedabia
        'V11': '', 
        'W12': '', 
        'X12': '', 
        # --- 13 ---
        'E4': 'escarpment', 
        'F4': 'escarpment', 
        'G5': '', 
        'H5': '', 
        'I6': 'escarpment', 
        'J6': 'escarpment', 
        'K7': 'escarpment', 
        'L7': '', 
        'M8': '', 
        'N8': '', 
        'O9': '', 
        'P9': '', 
        'Q10': '', 
        'R10': '', 
        'S11': '', 
        'T11': '', 
        'U12': '', 
        'V12': '', 
        'W13': '', 
        'X13': '', 
        # --- 14 ---
        'D4': '', 
        'E5': 'escarpment', 
        'F5': 'escarpment', 
        'G6': 'escarpment', 
        'H6': 'escarpment', 
        'I7': 'escarpment', 
        'J7': 'escarpment', 
        'K8': 'escarpment', 
        'L8': '', 
        'M9': '', 
        'N9': '', 
        'O10': '', 
        'P10': '', 
        'Q11': '', 
        'R11': '', 
        'S12': '', 
        'T12': '', 
        'U13': '', 
        'V13': '', 
        'W14': '', 
        'X14': '', 
        # --- 15 ---
    'C5': '', 
        'D5': 'escarpment', 
        'E6': '', 
        'F6': '', 
        'G7': 'escarpment', 
        'H7': 'escarpment', 
        'I8': 'escarpment', 
        'J8': 'escarpment', 
        'K9': '', 
        'L9': '', 
        'M10': '', 
        'N10': '', 
        'O11': '', 
        'P11': '', 
        'Q12': '', 
        'R12': '', 
        'S13': '', 
        'T13': '', 
        'U14': '', 
        'V14': '', 
        'W15': 'escarpment', 
        'X15': 'escarpment', 
        # --- 16 ---
        'B5': '', 
        'C6': 'escarpment', 
        'D6': 'escarpment', 
        'E7': 'escarpment', 
        'F7': '', 
        'G8': '', 
        'H8': 'escarpment', 
        'I9': '', 
        'J9': '', 
        'K10': '', 
        'L10': '', 
        'M11': '', 
        'N11': '', 
        'O12': '', 
        'P12': '', 
        'Q13': '', 
        'R13': '', 
        'S14': '', 
        'T14': '', 
        'U15': '', 
        'V15': 'escarpment', 
        'W16': '', 
        'X16': '', 
        # --- 17 ---
        'B6': '', 
        'C7': 'escarpment', 
        'D7': '', 
        'E8': '', 
        'F8': 'escarpment', 
        'G9': 'escarpment', 
        'H9': '', 
        'I10': '', 
        'J10': '', 
        'K11': '', 
        'L11': '', 
        'M12': '', 
        'N12': '', 
        'O13': '', # Msus
        'P13': '', 
        'Q14': '', 
        'R14': '', 
        'S15': '', 
        'T15': '', 
        'U16': 'escarpment', 
        'V16': 'escarpment', 
        'W17': '', 
        'X17': '', 
        # --- 18 ---
        'B7': '', 
        'C8': 'escarpment', 
        'D8': 'escarpment', 
        'E9': 'escarpment', 
        'F9': 'escarpment', 
        'G10': '', 
        'H10': '', 
        'I11': '', 
        'J11': '', 
        'K12': '', 
        'L12': '', 
        'M13': '', 
        'N13': '', 
        'O14': '', 
        'P14': '', 
        'Q15': '', 
        'R15': '', 
        'S16': '', 
        'T16': '', 
        'U17': 'escarpment', 
        'V17': '', 
        'W18': 'escarpment', 
        'X18': '', 
        # --- 19 ---
        'B8': '', 
        'C9': 'escarpment', 
        'D9': 'escarpment', 
        'E10': 'escarpment', 
        'F10': 'escarpment', 
        'G11': '', 
        'H11': '', 
        'I12': '', 
        'J12': '', 
        'K13': '', 
        'L13': '', 
        'M14': '', 
        'N14': '', 
        'O15': '', 
        'P15': '', 
        'Q16': '', 
        'R16': '', 
        'S17': '', 
        'T17': '', 
        'U18': 'escarpment', 
        'V18': '', 
        'W19': '', 
        'X19': 'escarpment', 
        # --- 20 ---
        'B9': '', 
        'C10': 'escarpment', 
        'D10': '', 
        'E11': 'escarpment', 
        'F11': 'escarpment', 
        'G12': '', 
        'H12': '', 
        'I13': '', 
        'J13': '', 
        'K14': '', 
        'L14': '', 
        'M15': '', 
        'N15': '', 
        'O16': '', 
        'P16': '', 
        'Q17': '', 
        'R17': '', 
        'S18': '', 
        'T18': '', 
        'U19': 'escarpment', 
        'V19': '', 
        'W20': '', 
        'X20': 'escarpment', 
        # --- 21 ---
        'B10': '', 
        'C11': 'escarpment', 
        'D11': 'escarpment', 
        'E12': '', 
        'F12': '', 
        'G13': '', 
        'H13': '', 
        'I14': '', 
        'J14': '', 
        'K15': '', 
        'L15': '', 
        'M16': '', 
        'N16': '', 
        'O17': '', 
        'P17': '', 
        'Q18': 'escarpment', 
        'R18': '', 
        'S19': '', 
        'T19': '', 
        'U20': '', 
        'V20': 'escarpment', 
        'W21': '', 
        'X21': 'escarpment', 
        # --- 22 ---
        'B11': '', 
        'C12': '', 
        'D12': '', 
        'E13': '', 
        'F13': '', 
        'G14': '', 
        'H14': '', 
        'I15': '', 
        'J15': '', 
        'K16': '', 
        'L16': '', 
        'M17': '', 
        'N17': '', 
        'O18': '', 
        'P18': 'escarpment', 
        'Q19': '', 
        'R19': 'escarpment', 
        'S20': 'escarpment', 
        'T20': '', 
        'U21': '', 
        'V21': '', 
        'W22': '', 
        'X22': 'escarpment', 
        # --- 23 ---
    'B12': '', 
        'C13': '', 
        'D13': '', 
        'E14': '', 
        'F14': '', 
        'G15': '', 
        'H15': '', #	Mechili
        'I16': '', 
        'J16': '', 
        'K17': '', 
        'L17': '', 
        'M18': '', 
        'N18': '', 
        'O19': '', 
        'P19': 'escarpment', 
        'Q20': 'escarpment', 
        'R20': 'escarpment', 
        'S21': '', 
        'T21': '', 
        'U22': '', 
        'V22': '', 
        'W23': '', 
        'X23': 'escarpment', 
        # --- 24 ---
        'B13': '', 
        'C14': '', 
        'D14': '', 
        'E15': '', 
        'F15': '', 
        'G16': '', 
        'H16': 'escarpment', 
        'I17': '', 
        'J17': '', 
        'K18': 'escarpment', 
        'L18': '', 
        'M19': '', 
        'N19': 'escarpment', 
        'O20': '', 
        'P20': '', 
        'Q21': '', 
        'R21': '', 
        'S22': '', 
        'T22': '', 
        'U23': '', 
        'V23': '', 
        'W24': '', 
        'X24': '', 
        # --- 25 ---
        'C15': '', 
        'D15': '', 
        'E16': '', 
        'F16': '', 
        'G17': '', 
        'H17': 'escarpment', 
        'I18': '', 
        'J18': '', 
        'K19': '', 
        'L19': 'escarpment', 
        'M20': 'escarpment', 
        'N20': 'escarpment', 
        'O21': 'escarpment', 
        'P21': '', 
        'Q22': 'escarpment', 
        'R22': '', 
        'S23': '', 
        'T23': '', 
        'U24': '', 
        'V24': '', 
        'W25': '', 
        'X25': '', 
        # --- 26 ---
        'C16': '', 
        'D16': '', 
        'E17': '', 
        'F17': 'escarpment', 
        'G18': 'escarpment', 
        'H18': 'escarpment', 
        'I19': '', 
        'J19': '', 
        'K20': '', 
        'L20': '', 
        'M21': 'escarpment', 
        'N21': '', 
        'O22': '', 
        'P22': 'escarpment', 
        'Q23': '', 
        'R23': 'escarpment', 
        'S24': 'escarpment', 
        'T24': '', 
        'U25': '', 
        'V25': '', 
        'W26': '', 
        'X26': '', 
        # --- 27 ---
    'C17': '', 
        'D17': '', 
        'E18': '', 
        'F18': '', #	Tmimi
        'G19': '', 
        'H19': 'escarpment', 
        'I20': '', 
        'J20': '', 
        'K21': '', 
        'L21': '', 
        'M22': '', 
        'N22': '', 
        'O23': '', 
        'P23': '', 
        'Q24': '', 
        'R24': '', 
        'S25': 'escarpment', 
        'T25': '', 
        'U26': '', 
        'V26': '', 
        'W27': '', 
        'X27': '', 
        # --- 28 ---
        'F19': '', 
        'G20': '', 
        'H20': '', 
        'I21': '', 
        'J21': '', 
        'K22': '', 
        'L22': '', 
        'M23': '', 
        'N23': '', 
        'O24': '', 
        'P24': '', 
        'Q25': '', 
        'R25': '', 
        'S26': '', 
        'T26': 'escarpment', 
        'U27': '', 
        'V27': '', 
        'W28': '', 
        'X28': '', 
        # --- 29 ---
        'G21': '', #	Gazala
        'H21': '', 
        'I22': '', 
        'J22': '', 
        'K23': '', 
        'L23': '', 
        'M24': '', 
        'N24': '', 
        'O25': '', 
        'P25': '', 
        'Q26': '', 
        'R26': '', 
        'S27': '', 
        'T27': 'escarpment', 
        'U28': '', 
        'V28': '', 
        'W29': '', 
        'X29': '', 
        # --- 30 ---
    'G22': '', 
        'H22': 'escarpment', 
        'I23': '', 
        'J23': '', 
        'K24': '', 
        'L24': '', 
        'M25': '', 
        'N25': '', 
        'O26': '', 
        'P26': '', 
        'Q27': '', 
        'R27': '', 
        'S28': '', 
        'T28': 'escarpment', 
        'U29': 'escarpment', 
        'V29': 'escarpment', 
        'W30': '', 
        'X30': '', 
        # --- 31 ---
        'G23': 'escarpment', 
        'H23': 'escarpment', 
        'I24': '', 
        'J24': '', 
        'K25': '', 
        'L25': '', # Bir Hacheim
        'M26': '', 
        'N26': '', 
        'O27': '', 
        'P27': '', 
        'Q28': 'escarpment', 
        'R28': '', 
        'S29': 'escarpment', 
        'T29': 'escarpment', 
        'U30': '', 
        'V30': '', 
        'W31': '', 
        'X31': '', 
        # --- 32 ---
    'G24': 'escarpment', 
        'H24': 'escarpment', 
        'I25': 'escarpment', 
        'J25': '', # Knightsbridge
        'K26': '', 
        'L26': '', 
        'M27': '', 
        'N27': '', 
        'O28': '', 
        'P28': 'escarpment', 
        'Q29': '', 
        'R29': 'escarpment', 
        'S30': '', 
        'T30': '', 
        'U31': '', 
        'V31': '', 
        'W32': '', 
        'X32': '', 
        # --- 33 ---
        'G25': 'fortress', #	Tobruk
        'H25': 'escarpment', 
        'I26': 'escarpment', 
        'J26': '', 
        'K27': '', 
        'L27': '', 
        'M28': '', 
        'N28': '', 
        'O29': '', 
        'P29': 'escarpment', 
        'Q30': '', 
        'R30': '', 
        'S31': '', 
        'T31': '', 
        'U32': '', 
        'V32': '', 
        'W33': '', 
        'X33': '', 
        # --- 34 ---
        'H26': 'escarpment', 
        'I27': 'escarpment', 
        'J27': '', # El Adam
        'K28': '', 
        'L28': '', # Bir El Gubi
        'M29': '', 
        'N29': '', 
        'O30': '', 
        'P30': '', 
        'Q31': '', 
        'R31': '', 
        'S32': '', 
        'T32': '', 
        'U33': '', 
        'V33': '', 
        'W34': '', 
        'X34': '', 
        # --- 35 ---
        'H27': 'escarpment', 
        'I28': 'escarpment', 
        'J28': '', 
        'K29': '', 
        'L29': '', 
        'M30': '', 
        'N30': '', 
        'O31': '', 
        'P31': '', 
        'Q32': '', 
        'R32': '', 
        'S33': '', 
        'T33': '', 
        'U34': '', 
        'V34': '', 
        'W35': '', 
        'X35': '', 
        # --- 36 ---
        'G28': 'escarpment', 
        'H28': 'escarpment', 
        'I29': 'escarpment', 
        'J29': '', 
        'K30': '', 
        'L30': '', 
        'M31': '', 
        'N31': '', 
        'O32': '', 
        'P32': '', 
        'Q33': '', 
        'R33': '', 
        'S34': '', 
        'T34': '', 
        'U35': '', 
        'V35': '', 
        'W36': '', 
        'X36': '', 
        # --- 37 ---
        'G29': 'escarpment', 
        'H29': '', 
        'I30': 'escarpment', 
        'J30': '', 
        'K31': '', 
        'L31': '', 
        'M32': '', 
        'N32': '', 
        'O33': '', 
        'P33': '', 
        'Q34': '', 
        'R34': '', 
        'S35': '', 
        'T35': '', 
        'U36': '', 
        'V36': '', 
        'W37': '', 
        'X37': '', 
        # --- 38 ---
        'G30': 'escarpment', 
        'H30': '', 
        'I31': 'escarpment', 
        'J31': '', 
        'K32': '', 
        'L32': '', 
        'M33': '', 
        'N33': '', 
        'O34': '', 
        'P34': '', 
        'Q35': '', 
        'R35': '', 
        'S36': '', 
        'T36': '', 
        'U37': '', 
        'V37': '', 
        'W38': '', 
        'X38': '', 
        # --- 39 ---
        'G31': 'escarpment', 
        'H31': '', 
        'I32': 'escarpment', 
        'J32': '', 
        'K33': '', # Sidi' Omar
        'L33': '', 
        'M34': '', 
        'N34': '', 
        'O35': '', # Maddalena
        'P35': '', 
        'Q36': '', 
        'R36': '', 
        'S37': '', 
        'T37': '', 
        'U38': '', 
        'V38': '', 
        'W39': '', 
        'X39': '', 
        # --- 40 ---
        'G32': '', 
        'H32': '', #	Bardi
        'I33': '', #	Salum
        'J33': 'escarpment', 
        'K34': '', 
        'L34': '', 
        'M35': '', 
        'N35': '', 
        'O36': '', 
        'P36': '', 
        'Q37': '', 
        'R37': '', 
        'S38': '', 
        'T38': '', 
        'U39': '', 
        'V39': '', 
        'W40': '', 
        'X40': '', 
        # --- 41 ---
        'J34': '', 
        'K35': 'escarpment', 
        'L35': '', 
        'M36': '', 
        'N36': '', 
        'O37': '', 
        'P37': '', 
        'Q38': '', 
        'R38': '', 
        'S39': '', 
        'T39': '', 
        'U40': '', 
        'V40': '', 
        'W41': '', 
        'X41': '', 
        # --- 42 ---
        'J35': '', 
        'K36': 'escarpment', 
        'L36': '', 
        'M37': '', 
        'N37': '', 
        'O38': '', 
        'P38': '', 
        'Q39': '', 
        'R39': '', 
        'S40': '', 
        'T40': '', 
        'U41': '', 
        'V41': '', 
        'W42': '', 
        'X42': '', 
        # --- 43 ---
        'J36': '', 
        'K37': '', 
        'L37': 'escarpment', 
        'M38': '', 
        'N38': '', 
        'O39': '', 
        'P39': '', 
        'Q40': '', 
        'R40': '', 
        'S41': '', 
        'T41': '', 
        'U42': '', 
        'V42': '', 
        'W43': '', 
        'X43': '', 
        # --- 44 ---
        'I37': '', 
        'J37': '', 
        'K38': '', 
        'L38': 'escarpment', 
        'M39': '', 
        'N39': '', 
        'O40': '', 
        'P40': '', 
        'Q41': '', 
        'R41': '', 
        'S42': '', 
        'T42': '', 
        'U43': '', 
        'V43': '', 
        'W44': '', 
        'X44': '', 
        # --- 45 ---
        'I38': '', 
        'J38': '', 
        'K39': '', 
        'L39': 'escarpment', 
        'M40': '', 
        'N40': '', 
        'O41': '', 
        'P41': '', 
        'Q42': '', 
        'R42': '', 
        'S43': '', 
        'T43': '', 
        'U44': '', 
        'V44': '', 
        'W45': '', 
        'X45': '', 
        # --- 46 ---
        'H38': '', 
        'I39': '', 
        'J39': '', 
        'K40': '', 
        'L40': 'escarpment', 
        'M41': 'escarpment', 
        'N41': '', 
        'O42': '', 
        'P42': '', 
        'Q43': '', 
        'R43': '', 
        'S44': '', 
        'T44': '', 
        'U45': '', 
        'V45': '', 
        'W46': '', 
        'X46': '', 
        # --- 47 ---
        'H39': '', #	Sidi Barrani
        'I40': '', 
        'J40': '', 
        'K41': '', 
        'L41': '', 
        'M42': 'escarpment', 
        'N42': '', 
        'O43': '', 
        'P43': '', 
        'Q44': '', 
        'R44': '', 
        'S45': '', 
        'T45': '', 
        'U46': '', 
        'V46': '', 
        'W47': '', 
        'X47': '', 
        # --- 48 ---
    'H40': '', 
        'I41': '', 
        'J41': '', 
        'K42': '', 
        'L42': '', 
        'M43': 'escarpment', 
        'N43': '', 
        'O44': '', 
        'P44': '', 
        'Q45': '', 
        'R45': '', 
        'S46': '', 
        'T46': '', 
        'U47': '', 
        'V47': '', 
        'W48': '', 
        'X48': '', 
        # --- 49 ---
        'H41': '', 
        'I42': '', 
        'J42': '', 
        'K43': '', 
        'L43': '', 
        'M44': 'escarpment', 
        'N44': '', 
        'O45': '', 
        'P45': '', 
        'Q46': '', 
        'R46': '', 
        'S47': '', 
        'T47': '', 
        'U48': '', 
        'V48': '', 
        'W49': '', 
        'X49': '', 
        # --- 50 ---
        'H42': '', 
        'I43': '', 
        'J43': '', 
        'K44': '', 
        'L44': 'escarpment', 
        'M45': '', 
        'N45': '', 
        'O46': '', 
        'P46': '', 
        'Q47': '', 
        'R47': '', 
        'S48': '', 
        'T48': '', 
        'U49': '', 
        'V49': '', 
        'W50': '', 
        'X50': '', 
        # --- 51 ---
        'H43': '', 
        'I44': '', 
        'J44': '', 
        'K45': '', 
        'L45': 'escarpment', 
        'M46': '', 
        'N46': '', 
        'O47': '', 
        'P47': '', 
        'Q48': '', 
        'R48': '', 
        'S49': '', 
        'T49': '', 
        'U50': '', 
        'V50': '', 
        'W51': '', 
        'X51': '', 
        # --- 52 ---
        'H44': '', 
        'I45': '', 
        'J45': '', 
        'K46': '', 
        'L46': 'escarpment', 
        'M47': 'escarpment', 
        'N47': '', 
        'O48': '', 
        'P48': '', 
        'Q49': '', 
        'R49': '', 
        'S50': '', 
        'T50': '', 
        'U51': '', 
        'V51': '', 
        'W52': '', 
        'X52': '', 
        # --- 53 ---
        'I46': '', 
        'J46': '', 
        'K47': '', 
        'L47': '', 
        'M48': 'escarpment', 
        'N48': '', 
        'O49': '', 
        'P49': '', 
        'Q50': '', 
        'R50': '', 
        'S51': '', 
        'T51': '', 
        'U52': '', 
        'V52': '', 
        'W53': '', 
        'X53': '', 
        # --- 54 ---
        'I47': '', 
        'J47': '', 
        'K48': '', 
        'L48': '', 
        'M49': 'escarpment', 
        'N49': '', 
        'O50': '', 
        'P50': '', 
        'Q51': '', 
        'R51': '', 
        'S52': '', 
        'T52': '', 
        'U53': '', 
        'V53': '', 
        'W54': '', 
        'X54': '', 
        # --- 55 ---
        'I48': '', #	Matruh
        'J48': '', 
        'K49': '', 
        'L49': '', 
        'M50': '', 
        'N50': 'escarpment', 
        'O51': '', 
        'P51': '', 
        'Q52': '', 
        'R52': '', 
        'S53': '', 
        'T53': '', 
        'U54': '', 
        'V54': '', 
        'W55': '', 
        # --- 56 ---
        'J49': '', 
        'K50': '', 
        'L50': '', 
        'M51': '', 
        'N51': 'escarpment', 
        'O52': 'escarpment', 
        'P52': '', 
        'Q53': '', 
        'R53': '', 
        'S54': '', 
        'T54': '', 
        'U55': '', 
        'V55': '', 
        # --- 57 ---
        'J50': '', 
        'K51': '', 
        'L51': '', 
        'M52': '', 
        'N52': '', 
        'O53': '', 
        'P53': '', 
        'Q54': '', 
        'R54': '', 
        'S55': '', 
        'T55': '', 
        'U56': '', 
        # --- 58 ---
        'I51': '', 
        'J51': '', 
        'K52': '', 
        'L52': '', 
        'M53': '', 
        'N53': '', 
        'O54': '', 
        'P54': '', 
        'Q55': '', 
        'R55': '', 
        'S56': '', 
        'T56': '', 
        # --- 59 ---
        'J52': '', 
        'K53': '', # Fuka#	Fuka
        'L53': '', 
        'M54': '', 
        'N54': '', 
        'O55': '', 
        'P55': '', 
        'Q56': '', 
        'R56': '', 
        'S57': '', 
        'T57': '', 
        # --- 60 ---
        'J53': '', 
        'K54': '', 
        'L54': '', 
        'M55': '', 
        'N55': '', 
        'O56': '', 
        'P56': '', 
        'Q57': '', 
        'R57': '', 
        'S58': '', 
        # --- 61 ---
        'J54': '', 
        'K55': '', 
        'L55': '', 
        'M56': '', 
        'N56': '', 
        'O57': '', 
        'P57': '', 
        'Q58': '', 
        'R58': '', 
        # --- 62 ---
        'J55': '', 
        'K56': '', # El Daba #	El Daba
        'L56': '', 
        'M57': '', 
        'N57': '', 
        'O58': '', 
        'P58': '', 
        'Q59': '', 
        'R59': '', 
        'V61': '', 
        'W62': '', 
        'X62': '', 
        # --- 63 ---
        'J56': '', 
        'K57': '', 
        'L57': '', 
        'M58': '', 
        'N58': '', 
        'O59': '', 
        'P59': '', 
        'Q60': '', 
        'S61': '', 
        'U62': '', 
        'V62': '', 
        'W63': '', 
        'X63': '', 
        # --- 64 ---
        'J57': '', 
        'K58': '', 
        'L58': '', 
        'M59': '', 
        'N59': 'escarpment', 
        'O60': '', 
        'P60': '', 
        'Q61': '', 
        'S62': '', 
        'T62': '', 
        'U63': '', 
        'V63': '', 
        'W64': '', 
        'X64': '', 
        # --- 65 ---
        'J58': '', 
        'K59': '', 
        'L59': '', # El Alamein
        'M60': '', 
        'N60': '', 
        'O61': 'escarpment', 
        'P61': '', 
        'Q62': '', 
        'S63': '', 
        'T63': '', 
        'U64': '', 
        'V64': '', 
        'W65': '', 
        'X65': '', 
        # --- 66 ---
        'K60': '', 
        'L60': '', 
        'M61': '', 
        'N61': 'escarpment', 
        'O62': 'escarpment', 
        'P62': '', 
        'Q63': '', 
        'S64': '', 
        'T64': '', 
        'U65': '', 
        'V65': '', 
        'W66': '', 
        'X66': '', 
        # --- 67 ---
        'K61': '', 
        'L61': '', 
        'M62': '', 
        'N62': 'escarpment', 
        'O63': '', 
        'P63': '', 
        'Q64': '', 
        'S65': '', 
        'T65': '', 
        'U66': '', 
        'V66': '', 
        'W67': '', 
        'X67': '', 
        # --- 68 ---
        'J61': '', 
        'K62': '', 
        'L62': '', 
        'M63': '', 
        'N63': '', 
        'O64': '', 
        'P64': '', 
        'Q65': '', 
        'R65': '', 
        'S66': '', 
        'T66': '', 
        'U67': '', 
        'V67': '', 
        'W68': '', 
        'X68': '', 
        # --- 69 ---
        'I62': '', 
        'J62': '={extra={[rotate=90]8th}}', 
        'K63': '', 
        'L63': '', 
        'M64': '', 
        'N64': '', 
        'O65': '', 
        'P65': '', 
        'Q66': '', 
        'R66': '', 
        'S67': '', 
        'T67': '', 
        'U68': '', 
        'V68': '', 
        'W69': '', 
        'X69': '', 
        # --- 70 ---
        'I63': '', 
        'J63': '', 
        'K64': '', 
        'L64': '', 
        'M65': '', 
        'N65': '', 
        'O66': '', 
        'P66': '', 
        'Q67': '', 
        'R67': '', 
        'S68': '', 
        'T68': '', 
        'U69': '', 
        'V69': '', 
    }
    escarpmentList = [h for h,v in hexes.items() if v=='escarpment']
    fortressList   = [h for h,v in hexes.items() if v=='fortress']
    escarpmentCode = ':'+':'.join(escarpmentList) + ':'
    fortressCode   = ':'+':'.join(fortressList)   + ':'
    
    # One row per odds, one column per die roll
    crt = {'1:7': ['AD', 'AD', 'AD', 'AD', 'AD', 'AD'], 
           '1:6': ['AE', 'AE', 'AE', 'AE', 'AE', 'AR'], 
           '1:5': ['AE', 'AE', 'AE', 'AE', 'AR', 'AR'], 
           '1:4': ['AE', 'AE', 'AE', 'AR', 'AR', 'AR'], 
           '1:3': ['AE', 'AE', 'AR', 'AR', 'AR', 'AR'],  
           '1:2': ['AE', 'AE', 'AR', 'AR', 'EX', 'DR'], 
           '1:1': ['AE', 'AE', 'AR', 'EX', 'DR', 'DE'], 
           '2:1': ['AE', 'AR', 'EX', 'EX', 'DR', 'DE'], 
           '3:1': ['EX', 'EX', 'DR', 'DR', 'DE', 'DE'], 
           '4:1': ['EX', 'DR', 'DR', 'DE', 'DE', 'DE'], 
           '5:1': ['DR', 'DR', 'DE', 'DE', 'DE', 'DE'], 
           '6:1': ['DR', 'DE', 'DE', 'DE', 'DE', 'DE'], 
           '7:1': ['AV', 'AV', 'AV', 'AV', 'AV', 'AV']}
        
    verb('Add Custom Java class')
    vmod.addExternalFile('ak/ObliqueHexGridNumbering.class')

    # Add help images
    imgs = ['.imgs/combat_1_begin.png',
            '.imgs/combat_2_odds.png',
            '.imgs/combat_3_result.png',
            '.imgs/combat_4_implement.png']
    for img in imgs:
        src = Path(img)
        dst = Path('images') / src.name
        vmod.addExternalFile(img,str(dst))

    # Change the hotkeys to closed windows to always so that units may
    # be moved out of OOB even if not open.
    gopts = game.getGlobalOptions()[0]
    gopts['hotKeysOnClosedWindows'] = 'Always'
    opts  = gopts.getOptions()

    verb('Add Global Property')
    notSetup    = 'NotInitialSetup'
    notDeclare  = 'NotDeclare'
    notResolve  = 'NotResolve'
    axis10rp    = 'Axis10rp'
    axis01rp    = 'Axis1rp'
    allied10rp  = 'Allied10rp'
    allied01rp  = 'Allied1rp'
    escarpments = 'Escarpments'
    fortresses  = 'Fortresses'
    glob = game.getGlobalProperties()[0]
    glob.addProperty(name         = notSetup,
                     initialValue = False,
                     description  = 'Whether we are not at the start')
    glob.addProperty(name         = notResolve,
                     initialValue = True,
                     description  = 'Whether we can resolve')
    glob.addProperty(name         = notDeclare,
                     initialValue = True,
                     description  = 'Whether we can declare')
    glob.addProperty(name         = axis10rp,
                     initialValue = 0,
                     isNumeric    = True,
                     min          = 0)
    glob.addProperty(name         = axis01rp,
                     initialValue = 0,
                     isNumeric    = True,
                     min          = 0)
    glob.addProperty(name         = allied10rp,
                     initialValue = 0,
                     isNumeric    = True,
                     min          = 0)
    glob.addProperty(name         = allied01rp,
                     initialValue = 0,
                     isNumeric    = True,
                     min          = 0)
    glob.addProperty(name         = escarpments,
                     initialValue = escarpmentCode,
                     isNumeric    = False)
    glob.addProperty(name         = fortresses,
                     initialValue = fortressCode,
                     isNumeric    = False)
    objectives = { 'AxisBase'  : 'W3',
                   'AlliedBase': 'J62',
                   'Tobruk'    : 'G25',
                   'Benghazi'  : 'H2' }
    for place in objectives:
        glob.addProperty(name         = f'{place}Control',
                         initialValue = 'None')
        glob.addProperty(name         = f'{place}Occupy',
                         initialValue = 'None')
    
    # ----------------------------------------------------------------
    # Extra documentation
    verb('Adding help file')
    doc  = game.getDocumentation()[0]
    doc.addHelpFile(title='More information',fileName='help/more.html')
    
    with open('help.html','r') as h:
        moreHelp = ''.join(h.readlines())
    
    vmod.addFile('help/more.html', moreHelp.format(title=game['name']))

    # ----------------------------------------------------------------
    # All maps 
    maps = game.getMaps()
    main = maps['Board']

    # ----------------------------------------------------------------
    # Get the restore global key, set icon and remove the deadmap map,
    # since we will move eliminated units to the OOBs instead.
    verb('Remove generated dead map')
    restore = maps['DeadMap'].getMassKeys()['Restore']
    restore['icon'] = 'restore-icon.png'
    game.remove(maps['DeadMap'])

    # ----------------------------------------------------------------
    # print(game.getDiceButtons())
    game.getSymbolicDices()['1d6Dice']['icon'] = 'dice-icon.png'
    # game.getDiceButtons()['1d6']['icon']       = 'dice-icon.png'
    game.getPieceWindows()['Counters']['icon'] = 'unit-icon.png'
    
    # ----------------------------------------------------------------
    # Set custom icons on some global keys 
    mkeys = main.getMassKeys()
    mkeys['Eliminate']['icon'] = 'eliminate-icon.png'
    userMark                 = mkeys['User mark battle']
    selMark                  = mkeys['Selected mark battle']
    resMark                  = mkeys['Selected resolve battle']
    userMark['canDisable']   = True
    userMark['propertyGate'] = notDeclare
    selMark ['canDisable']   = True
    selMark ['propertyGate'] = notDeclare
    resMark ['canDisable']   = True
    resMark ['propertyGate'] = notResolve
    
    # ----------------------------------------------------------------
    # Add global key to set isolated status
    verb('Adding some global keys')
    main.addMassKey(name         = 'Add isolated marker',
                    buttonHotkey = key('I'),
                    hotkey       = key('I'),
                    buttonText   = '', # g['name']
                    icon         = 'isolated-icon.png',
                    reportSingle = True,
                    reportFormat = '',
                    tooltip      = 'Add isolated marker')
    main.addMassKey(name         = 'Remove isolated marker',
                    buttonHotkey = key('F'),
                    hotkey       = key('F'),
                    buttonText   = '', # g['name']
                    icon         = 'supplied-icon.png',
                    reportSingle = True,
                    reportFormat = '',
                    tooltip      = 'Remove isolated marker from seleced')

    # ----------------------------------------------------------------
    # Modify the detail viewer
    viewer                 = main.getCounterDetailViewer(True)[0]
    viewer['bgColor']      = boardColor # Board background
    viewer['borderWidth']  = 2
    viewer['graphicsZoom'] = 1
    # viewer['delay']       = 2000
    main.addCounterDetailViewer(fontSize            = 16,
                                bgColor             = boardColor,
                                borderWidth         = 0,
                                delay               = 1000,
                                summaryReportFormat = '<b>$LocationName$</b>',
                                hotkey              =  key('\n'),
                                showgraph           = False,
                                minDisplayPieces    = 0)
                                

    # ----------------------------------------------------------------
    # Get Zoned area
    verb('Fixing up zones')
    zoned = main.getBoardPicker()[0].getBoards()['Board'].getZonedGrids()[0]
    # ----------------------------------------------------------------
    # Adjust the hexes
    #
    # Get the hex grid and turn of drawing
    zones  = zoned.getZones()
    hzone  = zones['hexes']
    hgrid  = hzone.getHexGrids()[0]
    hnum   = hgrid.getNumbering()[0]
    hgrid.remove(hnum)
    # Copy the old parameters
    hnattr = {k:v for k,v in hnum.getAttributes().items()}
    # Set new parameters xOff=89, yOff=73
    hgrid['y0']         = 73
    hnattr['hOff']      = 0
    hnattr['vOff']      = -9
    hgrid.addNode('ak.ObliqueHexGridNumbering',**hnattr)
    zoned.remove(hzone)
    zoned.append(hzone)

    # ----------------------------------------------------------------
    # Remove grid from 'at sea' zones
    with VerboseGuard('Remove grid from sea boxes') as v:
        for faction in ['axis', 'allied']:
            v(f'{faction} at sea')
            zone = zones[f'{faction} at sea']
            zone['locationFormat'] = '$name$'
            grid = zone.getGrids(single=True)[0]
            if zone.remove(grid) is None:
                v(f'Failed to remove grid from "{faction} at sea"')


    # ----------------------------------------------------------------
    verb('Define turns')
    turns                 = game.getTurnTracks()['Turn']
    turns['reportFormat'] = '{"<b>Phase</b>:  <u>"+Phase+"</u>"}'
    phaseNames            = ['Axis supply',
                             'Axis reinforcements',
                             'Axis movement',
                             'Axis combat',
                             'Axis victory',
                             'Allied supply',
                             'Allied reinforcements',
                             'Allied movement',
                             'Allied combat',
                             'Allied victory']
    phases                 = turns.getLists()['Phase']
    phases['list']         = ','.join(phaseNames)
    axisSupply             = phaseNames[0]
    axisReinf              = phaseNames[1]
    axisCombat             = phaseNames[3]
    axisElim               = phaseNames[4]
    alliedSupply           = phaseNames[5]
    alliedReinf            = phaseNames[6]
    alliedCombat           = phaseNames[-2]
    alliedElim             = phaseNames[-1]
    # ----------------------------------------------------------------
    # Define global keys for turn marker
    # 
    # - A key when moving from allied to german
    # - A key when moving from german to allied
    # - A key when entering the german turn so we can eliminate
    #   isolated Allied units
    # - A key when entering the Allied turn so we can mark out-of-supply
    #   Allied units with the isolated marker
    #
    # Below we define the corresponding global keys
    verb('Define turn handlers')
    elimTwice    = key(NONE,0)+',elimTwice' # key('E',CTRL_SHIFT)
    markTwice    = key(NONE,0)+',markTwice' # key('I',CTRL_SHIFT)
    reinforce    = key(NONE,0)+',reinforce'
    testInit     = key(NONE,0)+',testInitial' # key
    isInit       = key(NONE,0)+',isInitial'
    checkDeclare = key(NONE,0)+',isInitial'
    checkResolve = key(NONE,0)+',isInitial'
    elimKey      = key(NONE,0)+',eliminate'
    ctrlKey      = key(NONE,0)+',checkControl'
    occuKey      = key(NONE,0)+',checkOccupy'
    resetOccu    = key(NONE,0)+',resetOccupy'
    incrKey      = key(NONE,0)+',increment'
    decrKey      = key(NONE,0)+',decrement'
    replTurn     = 23
    turns.addHotkey(hotkey       = elimTwice,
                    match        = f'{{Phase=="{alliedElim}"}}',
                    name         = 'Allied attrition')
    turns.addHotkey(hotkey       = elimTwice,
                    match        = f'{{Phase=="{axisElim}"}}',
                    name         = 'Axis attrition')
    turns.addHotkey(hotkey       = markTwice,
                    match        = f'{{Phase=="{alliedSupply}"}}',
                    name         = 'Allied supply')
    turns.addHotkey(hotkey       = markTwice,
                    match        = f'{{Phase=="{axisSupply}"}}',
                    name         = 'Axis isolation')
    turns.addHotkey(hotkey       = ctrlKey+'GlobalAllied',
                    match        = (f'{{Turn>={replTurn}&&'
                                    f'Phase=="{alliedReinf}"}}'),
                    name         = 'Check control for Allied reinforcements')
    turns.addHotkey(hotkey       = ctrlKey+'GlobalAxis',
                    match        = (f'{{Turn>={replTurn}&&'
                                    f'Phase=="{axisReinf}"}}'),
                    name         = 'Check control for Axis reinforcements')
    turns.addHotkey(hotkey       = occuKey+'GlobalAllied',
                    match        = f'{{Phase=="{alliedSupply}"}}',
                    name         = 'Check occupation for Allied supply')
    turns.addHotkey(hotkey       = occuKey+'GlobalAxis',
                    match        = f'{{Phase=="{axisSupply}"}}',
                    name         = 'Check occupation for Axis supply')
    turns.addHotkey(hotkey       = testInit,
                    match        = '{true}',
                    name         = 'Test initial turn')
    turns.addHotkey(hotkey       = reinforce+'Global',
                    match        = f'{{Phase=="{alliedReinf}"}}',
                    reportFormat = '',#'! Allied reinforcements',
                    name         = 'Allied reinforcements')
    turns.addHotkey(hotkey       = reinforce+'Global',
                    match        = f'{{Phase=="{axisReinf}"}}',
                    reportFormat = '',#! Axis reinforcements',
                    name         = 'Axis reinforcements')
    turns.addHotkey(hotkey       = '',
                    match        = (f'{{Phase=="{alliedSupply}"||'
                                    f'Phase=="{axisSupply}"}}'),
                    reportFormat = ('~ <b>Remember</b> to check for '
                                    '<u>isolation</u>'))
    turns.addHotkey(hotkey       = '',
                    match        = (f'{{Phase=="{alliedCombat}"||'
                                    f'Phase=="{axisCombat}"}}'),
                    reportFormat = ('~ <b>Remember</b> to check for '
                                    '<u>in supply</u>'))
    # --- Clear markers phases ---------------------------------------
    keys = turns.getHotkeys(asdict=True)
    keys['Clear battle markers']['match'] = (
        f'{{Phase=="Axis movement"'
        f'||Phase=="Axis victory"'
        f'||Phase=="Allied movement"'
        f'||Phase=="Allied victory"'
        f'}}')
    
    # Add some global keys to the map
    #
    # - Flip turn marker (Ctrl+Shift+F)
    # - Mark twice out-of-supply Allied units (Ctrl+Shift+I)
    # - Eliminate twice isolated Allied units (Ctrl+Shift+E)
    verb('Define global keys for turn handlers')
    main.addMassKey(name         = 'Mark twice isolated Allied units',
                    buttonHotkey = markTwice,
                    hotkey       = key('I',CTRL),
                    buttonText   = '', 
                    target       = '',
                    filter       = f'{{Phase=="{alliedSupply}"&&'\
                                      'Faction=="Allied"&&Isolated_Level==2}')
    #
    main.addMassKey(name         = 'Mark twice isolated Axis units',
                    buttonHotkey = markTwice,
                    hotkey       = key('I',CTRL),
                    buttonText   = '', 
                    target       = '',
                    filter       = f'{{Phase=="{axisSupply}"&&'\
                                      'Faction=="Axis"&&Isolated_Level==2}')
    #
    main.addMassKey(name         = 'Eliminate twice isolated Allied units',
                    buttonHotkey = elimTwice,
                    hotkey       = key('E',CTRL),
                    buttonText   = '', 
                    target       = '',
                    filter       = f'{{Phase=="{alliedElim}"&&'\
                                      'Faction=="Allied"&&Isolated_Level==3}')
    #
    main.addMassKey(name         = 'Eliminate twice isolated Axis units',
                    buttonHotkey = elimTwice,
                    hotkey       = key('E',CTRL),
                    buttonText   = '', 
                    target       = '',
                    filter       = f'{{Phase=="{axisElim}"&&'\
                                      'Faction=="Axis"&&Isolated_Level==3}')
    # 
    main.addMassKey(name         = 'Axis setup',
                    icon         = 'ak-icon.png',
                    buttonHotkey = key('G',CTRL_SHIFT),
                    hotkey       = key(NONE,0)+f',startHex',
                    buttonText   = '',
                    target       = '',
                    singleMap    = False,
                    canDisable   = True,
                    propertyGate = notSetup,
                    reportFormat = '! <b>Load Axis setup</b>',
                    tooltip      = 'Load Axis setup',
                    filter       = '{Faction=="Axis"}')
    main.addMassKey(name         = 'Allied setup',
                    icon         = '8th-icon.png',
                    buttonHotkey = key('A',CTRL_SHIFT),
                    hotkey       = key(NONE,0)+f',startHex',
                    buttonText   = '',
                    target       = '',
                    singleMap    = False,
                    canDisable   = True,
                    propertyGate = notSetup,
                    reportFormat = '! <b>Load Allied setup</b>',
                    tooltip      = 'Load Allied setup',
                    filter       = '{Faction=="Allied"}')
    main.addMassKey(name         = 'Test for initial setup',
                    icon         = '',
                    buttonHotkey = testInit,
                    hotkey       = isInit,
                    buttonText   = '',
                    target       = '',
                    tooltip      = 'Test for initial setup',
                    filter       = '{BasicName == "game turn"}')
    # Calculate control for Allied
    main.addMassKey(name         = 'Test of control',
                    icon         = '',
                    buttonHotkey = ctrlKey+'GlobalAllied',
                    hotkey       = ctrlKey,
                    target       = '',
                    filter       = '{true}',
                    reportFormat = '')
    # Calculate control for Axis
    main.addMassKey(name         = 'Test of control',
                    icon         = '',
                    buttonHotkey = ctrlKey+'GlobalAxis',
                    hotkey       = ctrlKey,
                    target       = '',
                    filter       = '{true}',
                    reportFormat = '')
    # Report control for Allied
    main.addMassKey(name         = 'Test of control',
                    icon         = '',
                    buttonHotkey = ctrlKey+'GlobalAllied',
                    hotkey       = incrKey+'X',
                    target       = '',
                    singleMap    = False,
                    filter       = '{BasicName == "uk rp 1"}',
                    reportFormat = ('{"! <b>Allied</b> recieves <u>"+'
                                    '((AlliedBaseControl=="Allied"?2:0)'
                                    '+(TobrukControl=="Allied"?1:0))'
                                    '+" RP</u>s "'
                                    '+" - Allied Base="+AlliedBaseControl'
                                    '+" Tobruk="+TobrukControl'
                                    '}'))
    # Report control for Axis
    main.addMassKey(name         = 'Test of control',
                    icon         = '',
                    buttonHotkey = ctrlKey+'GlobalAxis',
                    hotkey       = incrKey+'X',
                    target       = '',
                    singleMap    = False,
                    filter       = '{BasicName == "de rp 1"}',
                    reportFormat = ('{"! <b>Axis</b> recieves <u>"+'
                                    '((AxisBaseControl=="Axis"?1:0)'
                                    '+(TobrukControl=="Axis"?1:0))'
                                    '+" RP</u>s "'
                                    '+" - Axis Base="+AxisBaseControl'
                                    '+" Tobruk="+TobrukControl'
                                    '}'))
    # Reset occupation for Axis
    main.addMassKey(name         = 'Reset of occupy',
                    icon         = '',
                    buttonHotkey = occuKey+'GlobalAxis',
                    hotkey       = resetOccu,
                    target       = '',
                    filter       = '{BasicName == "game turn"}',
                    reportFormat = '')
    # Reset occupation for Allied
    main.addMassKey(name         = 'Reset of occupy',
                    icon         = '',
                    buttonHotkey = occuKey+'GlobalAllied',
                    hotkey       = resetOccu,
                    target       = '',
                    filter       = '{BasicName == "game turn"}',
                    reportFormat = '')
    # Calculate occupation for Axis
    main.addMassKey(name         = 'Test of occupy',
                    icon         = '',
                    buttonHotkey = occuKey+'GlobalAxis',
                    hotkey       = occuKey,
                    target       = '',
                    filter       = '{true}',
                    reportFormat = '')
    # Calculate occupation for Allied
    main.addMassKey(name         = 'Test of occupy',
                    icon         = '',
                    buttonHotkey = occuKey+'GlobalAllied',
                    hotkey       = occuKey,
                    target       = '',
                    filter       = '{true}',
                    reportFormat = '')
    # Report occupation for Allied
    main.addMassKey(name         = 'Test of occupy Allied',
                    icon         = '',
                    buttonHotkey = occuKey+'GlobalAllied',
                    hotkey       = '',
                    target       = '',
                    filter       = '{false}',
                    reportFormat = ('{"~ <b>Allied</b> receives <u>"+'
                                    '((AxisBaseOccupy=="Allied"'
                                    '||TobrukOccupy=="Allied"'
                                    '||AlliedBaseOccupy=="Allied")?"one":"no")'
                                    '+"</u> supply unit "'
                                    '+"(remember to take it)"'
                                    '+" - Axis Base="+AxisBaseOccupy'
                                    '+" Allied Base="+AlliedBaseOccupy'
                                    '+" Tobruk="+TobrukOccupy'
                                    '}'))
    # Report occupation for Axis
    main.addMassKey(name         = 'Test of occupy Axis',
                    icon         = '',
                    buttonHotkey = occuKey+'GlobalAxis',
                    hotkey       = '',
                    target       = '',
                    filter       = '{false}',
                    reportFormat = ('{"~ <b>Axis</b> eligible for <u>"+'
                                    '((AxisBaseOccupy=="Axis"'
                                    '||TobrukOccupy=="Axis"'
                                    '||AlliedBaseOccupy=="Axis")?"one":"no")'
                                    '+"</u> supply unit pending die roll "'
                                    '+"(remember to take it)"'
                                    '+" - Axis Base="+AxisBaseOccupy'
                                    '+" Allied Base="+AlliedBaseOccupy'
                                    '+" Tobruk="+TobrukOccupy'
                                    '}'))
    # Check for declare
    main.addMassKey(name         = 'Test for declare battles',
                    buttonHotkey = checkDeclare,
                    hotkey       = checkDeclare,
                    target       = '',
                    filter       = '{BasicName=="game turn"}',
                    reportFormat = f'{{"Is not declare: "+{notDeclare}}}')
    # Check for resolve
    main.addMassKey(name         = 'Test for resolve battles',
                    buttonHotkey = checkResolve,
                    hotkey       = checkResolve,
                    target       = '',
                    filter       = '{BasicName=="game turn"}',
                    reportFormat = f'{{"Is not resolve: "+{notResolve}}}')

    # ----------------------------------------------------------------
    # Add restore to OOBs
    # Add incremenent and decrement to RP track
    # Fix up RP counter initial position
    with VerboseGuard(f'Fixing up OOBs') as vv:
        oobs            = game.getChartWindows()['OOBs']
        oobs['icon']    = 'oob-icon.png'
        pools = [('Allied',maps['Allied OOB'],alliedReinf, 'uk'),
                 ('Axis',  maps['Axis OOB'],  axisReinf,   'de')]
        for faction,pool,phase,shrt in pools:
            vv(f'{pool["mapName"]}')
            
            #pool['icon'] = 'pool-icon.png'
            #pool.getMassKeys()['Restore']['icon'] = 'restore-icon.png'
            pool.append(restore)
            pool.addMassKey(name         = 'Decrement 1',
                            buttonHotkey = key('-',ALT),
                            hotkey       = decrKey+'1',
                            buttonText   = '',
                            icon         = '/icons/32x32/go-previous.png',
                            reportSingle = True,
                            reportFormat = '',
                            target       = '',
                            filter       = f'{{BasicName == "{shrt} rp 1"}}',
                            tooltip      = 'Decrement RP counter(s)')
            pool.addMassKey(name         = 'Increment 1',
                            buttonHotkey = key('=',ALT_SHIFT),
                            hotkey       = incrKey+'1',
                            buttonText   = '',
                            icon         = '/icons/32x32/go-next.png',
                            reportSingle = True,
                            reportFormat = '',
                            target       = '',
                            filter       = f'{{BasicName == "{shrt} rp 1"}}',
                            tooltip      = 'Increment RP counter(s)')
            pool.addMassKey(name         = 'Reinforce',
                            buttonHotkey = reinforce+'Global',
                            hotkey       = reinforce,
                            buttonText   = '', 
                            target       = '',
                            reportFormat = '',
                            singleMap    = True,
                            filter       = (f'{{Phase=="{phase}"&&'\
                                            f'Faction=="{faction}"&&'
                                            f'Appearance==Turn}}'))
            
            # Fix-up RP markers
            atstarts = pool.getAtStarts(single=False)
            addto = {}
            for atstart in atstarts:
                # print(atstart['name'])
                if atstart['name'].endswith('rp 1'):
                    atstart['name']     = atstart['name'].replace('1','0')
                    atstart['location'] = atstart['location'].replace('1','0')
                    addto[atstart['name']] = atstart
            shrt = 'uk' if faction == 'Allied' else 'de'
            addto[f'{shrt} rp 0']\
                .addPiece(game.getPieces(asdict=True)[f'{shrt} rp 10'])
        


    # ----------------------------------------------------------------
    prototypeContainer = game.getPrototypes()[0]
    prototypes         = prototypeContainer.getPrototypes()
    
    # ----------------------------------------------------------------
    # Loop over turns, and create prototype for game turn marker to
    # move it along the turn track.  We do that by adding commands to
    # the turn track, which then delegates to the prototype.
    #
    # We add the prototypeContainer to the game turn marker later on.
    with VerboseGuard('Prototypes for turns') as v:
        turnPrototypes     = []
        for t in range(1,39):
            if t == 0: v(' ',end='')
            v(f'[{t}] ',end='')
            k  = key(NONE,0)+f',Turn{t}'
            turns.addHotkey(hotkey       = k,
                            match        = (f'{{Turn=={t}&&'
                                            f'Phase=="{phaseNames[0]}"}}'),
                            reportFormat = f'<b>=== Turn {t} ===</b>',
                            name         = f'Turn {t}')
            main.addMassKey(name         = f'Turn marker to {t}',
                            buttonHotkey = k,
                            hotkey       = k,
                            buttonText   = '', 
                            target       = '',
                            filter       = ('{BasicName == "game turn"}'))
            
            pn     = f'To turn {t}'
            traits = [ SendtoTrait(mapName     = 'Board',
                                   boardName   = 'Board',
                                   name        = '',
                                   restoreName = '',
                                   restoreKey  = '',
                                   zone        = f'Turn track',
                                   destination = 'R', #R for region
                                   region      = ('game turn' if t == 1 else
                                                  f'turn {t}'),
                                   key         = k,
                                   x           = 0,
                                   y           = 0),
                       BasicTrait()]
            prototypeContainer.addPrototype(name        = f'{pn} prototype',
                                            description = f'{pn} prototype',
                                            traits      = traits)
            turnPrototypes.append(pn)
        v('')

    # ----------------------------------------------------------------
    # Increment, decrement RPs
    verb(f'Increment/decrement RPs')
    for fac, faction in [['uk', 'Allied'],['de','Axis']]:
        iCode = f'{{"{fac} rp "+((RPCounter+1+10) % 10)}}'
        dCode = f'{{"{fac} rp "+((RPCounter-1+10) % 10)}}'
        val  = CalculatedTrait(name = 'RPCounter',
                               expression = 'Integer.parseInt(LocationName.replaceAll("[^0-9]",""))',
                               description = 'Calculate current value')
                               
        incr = SendtoTrait(mapName     = f'{faction} OOB',
                           boardName   = f'{faction} OOB',
                           name        = '',
                           restoreName = '',
                           restoreKey  = '',
                           destination = 'R', #R for region
                           region      = iCode,
                           key         = incrKey)
        decr = SendtoTrait(mapName     = f'{faction} OOB',
                           boardName   = f'{faction} OOB',
                           name        = '',
                           restoreName = '',
                           restoreKey  = '',
                           destination = 'R', #R for region
                           region      = dCode,
                           key         = decrKey)        
        rt = (f'{{"~ "+Faction+" RPs: "+BasicName.replaceAll("{fac} rp","")+'\
              f'" x "+RPCounter}}')
        rep    = ReportTrait(incrKey,decrKey,  report = rt)
        fac    = MarkTrait('Faction',faction)
        tpe    = MarkTrait('Type','RP')
        lim    = RestrictAccessTrait(sides=[],description='Cannot move')        
        traits = [val,incr,decr,fac,tpe,BasicTrait()]
        prototypeContainer.addPrototype(name        = f'{faction} RP',
                                        description = f'{faction} RP',
                                        traits      = traits)
        
    # ----------------------------------------------------------------
    # Add an inventory sorted on the unit supply and isolation status.
    verb('Inventory by state')
    disp = ('{PropertyValue==Faction ? Faction : '
            'Isolated_Level==1 ? "In-supply" : ' + \
            'Isolated_Level==2 ? "Isolated" : "Twice isolated"}')
    game.addInventory(include       = ('{(Faction=="Allied"||Faction=="Axis")&&'
                                       '(Type!="supply"&&Type!="RP")}'),
                      groupBy       = 'Faction,Isolated_Level',
                      sortFormat    = '$PieceName$',
                      tooltip       = 'Show supply status of pieces',
                      nonLeafFormat = disp, #'$PropertyValue$',
                      zoomOn        = True,
                      hotkey        = key('S',ALT),
                      refreshHotkey = key('S',ALT_SHIFT),
                      icon          = 'isolated-icon.png')

    # ----------------------------------------------------------------
    # Dominion prototype
    ctrls    = []
    ctrlActs = []
    occu     = []
    occuActs = []
    for glob,hx in objectives.items():
        cc = f'{{LocationName=="{hx}"?Faction:{glob}Control}}'
        oo = f'{{LocationName=="{hx}"?Faction:{glob}Occupy}}'
        ck = key(NONE,0)+f',control{glob}'
        ok = key(NONE,0)+f',occupy{glob}'
        cp = GlobalPropertyTrait(('',ck,'P',cc),   name=glob+'Control')
        op = GlobalPropertyTrait(('',ok,'P',oo),   name=glob+'Occupy')
        ctrls   .append(cp)
        ctrlActs.append(ck)
        occu    .append(op)
        occuActs.append(ok)
    trgCtrl = TriggerTrait(name       = 'Check for control',
                           key        = ctrlKey,
                           actionKeys = ctrlActs)
    trgOccu = TriggerTrait(name       = 'Check for occupation',
                           key        = occuKey,
                           actionKeys = occuActs)
    rt = 'Checking control or occupation: $LocationName$ - $PieceName$'
    repCO = ReportTrait(*ctrlActs,*occuActs, report = rt)
    traits = ctrls+occu+[trgCtrl,trgOccu,BasicTrait()]
    prototypeContainer.addPrototype(name        = f'Dominion prototype',
                                    description = f'Check control/occupy',
                                    traits      = traits)
    
    # ----------------------------------------------------------------
    # Battle calculations
    battleUnitP = prototypes['wgBattleUnit']
    traits      = battleUnitP.getTraits()
    basic       = traits.pop()
    effdf       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name', value='EffectiveDF')
    effdf['expression'] = '{DF*(InEscarpment?2:1)*(InFortress?2:1)}'
    repdf       = None
    for trait in traits:
        if trait.ID != ReportTrait.ID:
            continue
        if 'EffectiveDF' not in trait['report']:
            continue
        repdf = trait
        break
    repdf['report'] = ('{wgVerbose?("! "+BasicName+" add "+EffectiveDF+'
                       '"to total defence factor ("+DF+'
                       '",escarpment="+InEscarpment+'
                       '",fortress="+InFortress+'
                       '","+LocationName+")"):""}')
    traits.extend([
        CalculatedTrait(
            name       = 'InEscarpment',
            expression = f'{{{escarpments}.contains(":"+LocationName+":")}}'),
        CalculatedTrait(
            name       = 'InFortress',
            expression = f'{{{fortresses}.contains(":"+LocationName+":")}}'),
        RestrictCommandsTrait(
            name          = 'Restrict Ctrl-X to movement and combat',
            hideOrDisable = RestrictCommandsTrait.DISABLE,
            expression    = f'{{{notDeclare}}}',
            keys          = [key('X')]),
        basic
    ])
    battleUnitP.setTraits(*traits)
        
    # ----------------------------------------------------------------
    # Battle calculations - I4  H2
    battleOddsP = prototypes['OddsMarkers prototype']
    traits      = battleOddsP.getTraits()
    basic       = traits.pop()
    battleres   = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name', value='BattleResult')
    die         = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='Die')
    battleres['expression'] = '{OddsResult}'
    die['expression']       = '{{GetProperty("1d6Dice_result")}}'
    traits.extend([
        RestrictCommandsTrait(
            name          = 'Restrict Ctrl-Y to combat',
            hideOrDisable = RestrictCommandsTrait.DISABLE,
            expression    = f'{{{notResolve}}}',
            keys          = [key('Y')]),
        basic])
    battleOddsP.setTraits(*traits)
        
    
    pieces = game.getPieces(asdict=True)
    for pn, piece in pieces.items():
        if not pn.startswith('odds marker'):
            continue

        odds = pn.replace('odds marker','').strip()
        row  = crt[odds]
        calc = ':'.join([f'(Die=={n+1})?"{r}"' for n,r in enumerate(row)])+':""'
        # print(calc)
        
        traits = piece.getTraits()
        basic  = traits.pop()
        traits.extend([
            CalculatedTrait(
                name='OddsResult',
                expression = f'{{{calc}}}'),
            basic])
        piece.setTraits(*traits)
    

    # ----------------------------------------------------------------
    #
    # Remove 'Eliminate' trait from faction traits.  We will replace
    # that trait on each unit with a trait that moves the unit back to
    # its place on the OOB.
    #
    # We create the traits once, since they will be copied to the prototypes
    # Phases to allow the isolate command
    verb('Faction protoypes')
    enableIsolate   = {'Allied':   [alliedSupply],
                       'Axis':     [axisSupply] }
    enableReset     = {'Allied':   [alliedCombat],
                       'Axis':     [axisCombat] }
    restrictIsolate = RestrictCommandsTrait(name='Restrict isolated command',
                                            hideOrDisable = 'Disable',
                                            expression = '',
                                            keys = [key('I')])
    restrictReset   = RestrictCommandsTrait(name='Restrict reset command',
                                            hideOrDisable = 'Disable',
                                            expression = '',
                                            keys       = [key('F')])

    isolated = LayerTrait(['',
                           'isolated-mark.png',
                           'twice-isolated-mark.png'],
                          ['','Isolated +','Twice isolated +'],
                          activateName = '',
                          activateMask = '',
                          activateChar = '',
                          increaseName = 'Isolated',
                          increaseMask = CTRL,
                          increaseChar = 'I',
                          decreaseName = '',
                          decreaseMask = '',
                          decreaseChar = '',
                          resetName    = 'Supplied',
                          resetKey     = key('F'),
                          under        = False,
                          underXoff    = 0,
                          underYoff    = 0,
                          loop         = False,
                          name         = 'Isolated',
                          description  = '(Un)Mark unit as isolated',
                          always       = False,
                          activateKey  = '',
                          increaseKey  = key('I'),
                          decreaseKey  = '',
                          scale        = 1)
    repIso  = ReportTrait(key('F'),key('I'),
                          report = ('{{BasicName+" isolation level: '
                                    '"+Isolated_Level}}'))
    actions = [elimKey,key('F')]
    trigger = TriggerTrait(name       = 'Eliminate unit',
                           command    = 'Eliminate',
                           key        = key('E'),
                           actionKeys = actions)
    toTobruk   = SendtoTrait(name        = 'To Tobruk',
                             mapName     = 'Board',
                             boardName   = 'Board',
                             key         = key(NONE,0)+',toTobruk',
                             restoreName = '',
                             restoreKey  = '',
                             destination = 'G',
                             position    = convHex('G25'))
    baseHex  = {'Allied': convHex('J62'), 
                'Axis':   convHex('W3') }
    toBase   = SendtoTrait(name        = 'To Base',
                           mapName     = 'Board',
                           boardName   = 'Board',
                           key         = key(NONE,0)+',toBase',
                           restoreName = '',
                           restoreKey  = '',
                           destination = 'G',
                           position    = 'G25')
    repBase  = ReportTrait(key(NONE,0)+',toBase',key(NONE,0)+',toTobruk',
                           report = '{{BasicName+" send to "+LocationName}}')
    reinfHex = {'Allied': convHex('H60'),
                'Axis':   convHex('T2') }
    reinf    = SendtoTrait(name        = '',
                           key         = reinforce,
                           mapName     = 'Board',
                           boardName   = 'Board',
                           destination = 'G',
                           restoreName = '',
                           restoreKey  = '',
                           position    = '')
    repReinf = ReportTrait(reinforce,
                           report = '!  Reinforcement $newPieceName$')
    dominion = PrototypeTrait('Dominion prototype')
    
    # Now modify the faction prototypes
    trails = {}
    for faction in ['Allied', 'Axis']:
        prototype = prototypes[f'{faction} prototype']
        traits    = prototype.getTraits()
        basic     = traits.pop()
        etrait    = Trait.findTrait(traits,SendtoTrait.ID,
                                    'name','Eliminate')
        trail     = Trait.findTrait(traits,TrailTrait.ID)
        trails[faction] = trail;
        if etrait is not None:
            etrait['name']        = ''
            etrait['key']         = elimKey
            etrait['destination'] = 'Z'
            etrait['zone']        = f'{faction} pool'
            etrait['boardName']   = f'{faction} OOB'
            etrait['mapName']     = f'{faction} OOB'
        
        if trail is not None:
            trail['lineColor'] = trailColors.get(faction,rgb(0,0,0))
            trail['fillColor'] = trailColors.get(faction,rgb(0,0,0))
        else:
            print(f'Missing trail for faction {faction}')
            
        phases  = enableIsolate.get(faction,None)
        restrictIsolate['expression'] = ''
        if phases is not None:
            restrictIsolate['expression'] = \
                '{!(' + '||'.join(f'Phase=="{p}"' for p in phases) + ')}'

        phases = enableReset.get(faction,None)
        restrictReset['expression'] = ''
        if phases is not None:
            restrictReset['expression'] = \
                '{!(' + '||'.join(f'Phase=="{p}"' for p in phases) + ')'+\
                '&&CurrentBoard=="Board"}'
        
        toBase['position']   = baseHex[faction]
        reinf ['position']   = reinfHex[faction]
        traits.extend([restrictReset,
                       restrictIsolate,
                       isolated,
                       repIso,
                       trigger,
                       reinf,
                       repReinf,
                       toTobruk,
                       toBase,
                       repBase,
                       dominion,
                       basic])            
        prototype.setTraits(*traits)

    # ----------------------------------------------------------------
    # Get all pieces and add Eliminate action that moves the unit to
    # the OOB. This is also where we add the traits to move the game
    # turn marker on the turn track.
    #incr10Key = key(NONE,0)+',incr10'
    #decr10Key = key(NONE,0)+',decr10'
    with VerboseGuard('Fixing up pieces') as v:
        pieces  = game.getPieces(asdict=False)
        axes    = ['de ','it ']
        allies  = ['uk ','fr ','pl ','jw ','nz ','in ','sa ','au ']
        for num,piece in enumerate(pieces):
            if num == 0: v('',end='')
            name    = piece['entryName'].strip()
            faction = name[:3]
            traits  = piece.getTraits()
            basic   = traits.pop()

            v(f'[{name}] ',end='')
            
            # Axis and Allied pieces
            if faction in axes+allies:
                factionName = 'Allied' if faction in allies else 'Axis'
                
                if 'supply' in name or ' rp ' in name:
                    ftrait = Trait.findTrait(traits,PrototypeTrait.ID,
                                             'name',f'{factionName} prototype')
                    if ftrait is not None:
                        traits.remove(ftrait)

                if 'supply' in name:
                    trail = trails.get(factionName,None)
                    if trail is None:
                        print(f'Missing trail to set on {name}')
                    else:
                        traits.append(trail)
        
                if ' rp ' in name:
                    traits.append(PrototypeTrait(factionName + ' RP'))
                    glob = f'{factionName}{name.replace(faction+"rp ","")}rp'
                    setg = GlobalPropertyTrait(('',incrKey,'P','{RPCounter}'),
                                               ('',decrKey,'P','{RPCounter}'),
                                               name    = glob,
                                               numeric = True)
                    if name.endswith(' 1'):
                        incX = (f'{{(((AlliedBaseControl=="Allied")?2:0)'
                                f'+((TobrukControl=="Allied")?1:0))}}'
                                if 'uk' in faction else
                                f'{{(((AxisBaseControl=="Axis")?1:0)'
                                f'+((TobrukControl=="Axis")?1:0))}}')
                        incr10 = (f'{{BasicName == "{faction}rp 10" '
                                  f'&& $RPCounter$==9}}')
                        decr10 = (f'{{BasicName == "{faction}rp 10" '
                                  f'&& $RPCounter$==0}}')
                        drp = CalculatedTrait(name='RPIncrement',
                                              expression=incX,
                                              description="RP increase")
                        aiX = TriggerTrait(name = 'Trigger X increment',
                                           key           = incrKey+'X',
                                           actionKeys    = [incrKey+'1'],
                                           loop          = True,
                                           loopType      = TriggerTrait.WHILE,
                                           index         = True,
                                           indexProperty = 'IncrLoop',
                                           indexStart    = 0,
                                           indexStep     = 1,
                                           whileExpression = '{RPIncrement>IncrLoop}')
                        ai1 = TriggerTrait(name = 'Trigger increment',
                                            command='+1',
                                            key  = incrKey+'1',
                                            actionKeys = [incrKey+'10',incrKey])
                        ad1 = TriggerTrait(name = 'Trigger decrement',
                                            command='-1',
                                            key  = decrKey+'1',
                                            actionKeys = [decrKey+'10',decrKey])
                        i10 = GlobalCommandTrait(commandName = '',
                                                 key         = incrKey+'10',
                                                 globalKey   = incrKey,
                                                 properties  = incr10,
                                                 description = 'Increment 10')
                        d10 = GlobalCommandTrait(commandName = '',
                                                 key         = decrKey+'10',
                                                 globalKey   = decrKey,
                                                 properties  = decr10,
                                                 description = 'Decrement 10')
                        inc = [ai1]
                        dec = [ad1]
                        for i in range(2,8):
                            ai = TriggerTrait(name = f'Trigger {i} increment',
                                              command=f'+{i}',
                                              key  = incrKey+str(i),
                                              actionKeys = [incrKey+'1',
                                                            incrKey+f'{i-1}'])
                            di = TriggerTrait(name = f'Trigger {i} decrement',
                                              command=f'-{i}',
                                              key  = decrKey+str(i),
                                              actionKeys = [decrKey+'1',
                                                            decrKey+f'{i-1}'])
                            inc.append(ai)
                            dec.append(di)
                        rt  = (f'{{"! {factionName} RPs: "'
                               f'+(10*Integer.parseInt({factionName}10rp.toString())'
                               f'+Integer.parseInt({factionName}1rp.toString()))'
                               f'+" ("+RPIncrement+")"}}'
                               )
                        rep = ReportTrait(incrKey,decrKey,
                                          report = rt)
                        traits.extend([drp]+[i10,d10]+inc[::-1]+dec+
                                      [aiX,setg,rep])
                    else:
                        traits.append(setg)
                            
                # Eliminate trait - supply units are sent back to OOB,
                # not dead pool.
                if 'supply' in name:
                    factionMark = MarkTrait('Faction',factionName)
                    oob         = f'{factionName} OOB'
                    elim        = SendtoTrait(name        = 'Eliminate',
                                              mapName     = oob,
                                              boardName   = oob,
                                              key         = key('E'),
                                              restoreName = f'Restore {name}',
                                              restoreKey  = key('R'),
                                              destination = 'R',
                                              region      = name+'@'+oob)
                    toBase['position'] = convHex('J62'
                                                 if factionName == 'Allied' 
                                                 else 'W3')
                    traits.extend([factionMark,elim,toTobruk,toBase,dominion])
                    
        
                # Get the start-up hex from the upper right mark
                hx    = None
                tn    = None
                start = Trait.findTrait(traits,MarkTrait.ID,
                                        'name', 'upper right')
                if start is not None:
                    st = start['value']
                    st = sub('[^=]+=','',st).strip()
                    hx = convHex(st)
                    if hx is None:
                        try:
                            tn = int(st)
                        except:
                            tn = None

                # If we have a start up hex, create trait
                # print(f'{name} -> start = {hx}')
                if hx is not None:
                    start['name']  = 'Start'
                    start['value'] = hx
                    startKey = key(NONE,0)+',startHex'
                    startup = SendtoTrait(name        = '',
                                          key         = startKey,
                                          mapName     = 'Board',
                                          boardName   = 'Board',
                                          destination = 'G',
                                          restoreName = '',
                                          restoreKey  = '',
                                          position    = hx)
                    rt     = '!  $newPieceName$ moved to start hex $location$'
                    report = ReportTrait(startKey,report = rt)
                    traits.extend([startup,report])
                elif tn is not None:
                    start['name']  = 'Appearance'
                    start['value'] = tn
                    # traits.append(MarkTrait('Appearance',str(tn))) 
        
        
            # Add prototype traits to game turn marker.  Note, we
            # remove the basic piece trait from the end of the list,
            # and add it in after adding the other traits: The basic
            # piece trait _must_ be last.
            elif name == 'game turn':
                mtrait  = Trait.findTrait(traits,PrototypeTrait.ID,
                                          'name','Markers prototype')
                if mtrait is not None:
                    traits.remove(mtrait)
        
                rtrait  = Trait.findTrait(traits,ReportTrait.ID)
                if rtrait is not None:
                    traits.remove(rtrait)
        
                traits.extend([PrototypeTrait(pnn + ' prototype')
                               for pnn in turnPrototypes])
        
                traits.append(GlobalPropertyTrait(
                    ['',isInit,GlobalPropertyTrait.DIRECT,
                     f'{{Turn!=1||Phase!="{axisSupply}"}}'
                     ],
                    name = notSetup))
                traits.append(GlobalPropertyTrait(
                    ['',checkDeclare,GlobalPropertyTrait.DIRECT,
                     f'{{!(Phase.contains("movement")||'
                     f'Phase.contains("combat"))}}'],
                    name = notDeclare))
                traits.append(GlobalPropertyTrait(
                    ['',checkResolve,GlobalPropertyTrait.DIRECT,
                     f'{{!(Phase.contains("combat"))}}'],
                    name = notResolve))
                for o in objectives:
                    traits.append(GlobalPropertyTrait(
                        ['',resetOccu,GlobalPropertyTrait.DIRECT,'{"None"}'],
                        name = f'{o}Occupy'))
                traits.append(ReportTrait(resetOccu,
                                          report='Reset occupy properties'))
        
                #traits.append(RestrictAccessTrait(sides=[],
                #                                  description='Cannot move'))
                # print('Modifying the game turn marker',traits)
        
            # Add basic trait as last trait and set traits
            traits.append(basic)
            piece.setTraits(*traits)

        v('')    
#
# EOF
#

                      
    
    
