# Afrika Korps

[[_TOC_]]

This is my remake of the wargame _Afrika Korps_.  The original game
was published by Avalon Hill Game Company in 1964.  This remake is
based on the third edition of the game from 1980. 

Also check out its sister game:
[D-Day](https://gitlab.com/wargames_tex/dday_tex/). 

Other games are available from [L<sup>A</sup>T<sub>E</sub>X
Wargames](https://wargames_tex.gitlab.io/wargames_www). 

## Game mechanics 

|              |                    |
|--------------|--------------------|
| Period       | WWII               |
| Level        | Operational        |
| Hex scale    | 7.4km (11.9 miles) |
| Unit scale   | brigade (x)        |
| Turn scale   | half-month         |
| Unit density | Low                |
| # of turns   | 38                 |
| Complexity   | 2 of 10            |
| Solitaire    | 7 of 10            |

Features:
- Dessert war 
- Logistics
- Positional 

## About this rework 

This rework is entirely new.  All text and graphics is new and nothing
is copied verbatim from the original materials. 

I have restructured and rewritten the rules somewhat (the text is all
new), as well as added illustrations of the rules.

## The board 

The board is quite big (37.5cm tall and 121.4cm wide), and does not
fit into readily available printers.  The board is therefore presented
as a 5-part document.  There are two version: 

- One designed to be printed on A3 paper ([splitboardA3.pdf][]).  This
  holds a relatively comfortable size of counters.  
  
- One designed to be printed on A4 paper ([splitboardA4.pdf][]).  This
  further split into to two giving 10 parts. 
  
(for US Tabloid and Letter, replace `A3` with `Tabloid` and `A4` with
`Letter`). 
  
Print your PDF of choice onto 5 or 10 separate pieces of paper (if you
have a duplex printer, simply print the document) and glue on to
sturdy cardboard (I like 1 1/2mm poster cardboard).  Cut along dashed
crop lines.  You can hold the 5 or 10 pieces together with paper
clamps or the like.

Alternatively you can glue the 5 or 10 sheets of paper together.  Cut
on end along the crop marks (dashed lines) and glue on to the next
part, taking care to align the crop mark arrows.  When playing the
game, put a transparent heavy plastic sheet on top to hold the board
down.

Remember to use the appropriate `materialsX.pdf` file for your board. 

| Board<br>Paper | Board<br>file | Materials<br>file | Materials<br>paper  |
|---------|---------------------------|--------------------------|--------|
| A3      | [splitboardA3.pdf][]      | [materialsA4.pdf][]      | A4     |
| A4      | [splitboardA4.pdf][]      | [materialsA4.pdf][]      | A4     |
| Tabloid | [splitboardTabloid.pdf][] | [materialsLetter.pdf][]  | Letter |
| Letter  | [splitboardLetter.pdf][]  | [materialsLetter.pdf][]  | Letter |

If an A3 print is not available, and the 10 part board is
unmanageable, then one can scale down [splitboardA3.pdf][] by a factor
of $`1/\sqrt{2}=0.7071`$ to get a document that can be printed on an
A4 printer.  In that case, one should _also_ scale down
[materialsA4.pdf][] by the same factor and use the counters and OOB
form that scaled down copy.

To scale down for Letter paper, one should apply a scale factor of
$`0.65`$ to both [splitboardTabloid.pdf][] and
[materialsLetter.pdf][]. 

## The files 

The distribution consists of the following files 

- [AfrikaKorpsA4.pdf][] This is a single document that contains
  everything: The rules, charts, counters, and order of battle, and
  the board split into 10 parts.
  
  This document is meant to be printed on a duplex A4 printer.  The
  charts, OOB charts, counters, and board will be on separate sheets
  and can be separated from the rules.
  
  Note that the PDF is a little heavy to load (due to the large map),
  so a bit of patience is needed. 
  
- [AfrikaKorpsA4Booklet.pdf][] This document _only_ contains the
  rules.  It is meant to be printed on a duplex A4 printer with long
  edge binding.  This will produce 6 sheets which should be folded
  down the middle of the long edge, and stabled to form an 24-page A5
  booklet of the rules.
  
- [splitboardA3.pdf][] holds the board in 5 sheets of A3 paper.  Print
  these and glue on to a sturdy piece of cardboard.  You can perhaps
  make some clever arrangement that allows you to unfold the map.
  Otherwise, use paper clambs or the like to hold the board pieces
  together.  
  
- [splitboardA4.pdf][] is like [splitboardA3.pdf][] above, but further
  split to fit on 10 sheets of A4 paper.   
  
- [materialsA4.pdf][] holds the charts, counters, and OOB.  It is
  meant to be printed on A4 paper.  Print and glue on to   a
  relatively thick piece of cardboard (1.5mm or so) or the like. 

- [hexes.pdf][] is the entire board in one PDF.  If you have a way of
  printing such a large sheet, perhaps this can be of use to you. 
  

If you only have access to US Letter (and possibly Tabloid) printer,
you can use the following files

| *A4 Series*                  | *Letter Series*                     |
| -----------------------------|-------------------------------------|
| [AfrikaKorpsA4.pdf][]        | [AfrikaKorpsLetter.pdf][]  	     |
| [AfrikaKorpsA4Booklet.pdf][] | [AfrikaKorpsLetterBooklet.pdf][]    |
| [materialsA4.pdf][]		   | [materialsLetter.pdf][]			 |
| [splitboardA4.pdf][]		   | [splitboardLetter.pdf][]			 |
| [splitboardA3.pdf][]		   | [splitboardTabloid.pdf][]			 |

Note, the US letter files are scaled down to 95% relative to the A4
files.  This is to fit everything in on the same scale.  This should
not be a problem for most users as most of the world is using A4
rather than the rather obscure Letter format. 

Download [artifacts.zip][] to get all files for both kinds of paper
formats. 

## "Old-school" version 

This version mimics the original graphics.  That is, it uses NATO
_friendly_ and _hostile_ colours for UK and Axis units,
respectively. Otherwise, it is the same as above. 

| *A4 Series*                    | *Letter Series*                       |
| -------------------------------|---------------------------------------|
| [AfrikaKorpsA4OS.pdf][]        | [AfrikaKorpsLetterOS.pdf][]  	     |
| [AfrikaKorpsA4BookletOS.pdf][] | [AfrikaKorpsLetterBookletOS.pdf][]    |
| [materialsA4OS.pdf][]		     | [materialsLetterOS.pdf][]			 |
| [splitboardA4OS.pdf][]		 | [splitboardLetterOS.pdf][]			 |
| [splitboardA3OS.pdf][]		 | [splitboardTabloidOS.pdf][]			 |

## VASSAL Module 

There is a [VASSAL](https://vassalengine.org) module available 

- [AfrikaKorps.vmod][] 
- [AfrikaKorpsOS.vmod][] (old-school version)

This is made from the same sources as the Print'n'Play documents. 

## Previews 

[![The board](.imgs/hexes.png)](https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/hexes.pdf?job=dist "The board")
[![The rules](.imgs/front.png)](https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/AfrikaKorpsA4.pdf?job=dist)
[![The charts](.imgs/tables.png)](https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/materialsA4.pdf?job=dist)
[![The Allied OOB](.imgs/al-oob.png)](https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/materialsA4.pdf?job=dist)
[![The Axis OOB](.imgs/ax-oob.png)](https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/materialsA4.pdf?job=dist)
[![The counters](.imgs/counters.png)](https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/materialsA4.pdf?job=dist)
[![VASSAL module](.imgs/vassal.png)](https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/AfrikaKorps.vmod?job=dist)
![Build](.imgs/photo_components.jpg)
![Rommel leading the way](.imgs/photo_detail.jpg)

### Ol'School version 

[![The board](.imgs/hexesOS.png)](https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master+OS/hexes.pdf?job=dist "The board")
[![The rules](.imgs/frontOS.png)](https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master+OS/AfrikaKorpsA4.pdf?job=dist)
[![The charts](.imgs/tablesOS.png)](https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master+OS/materialsA4.pdf?job=dist)
[![The Allied OOB](.imgs/al-oobOS.png)](https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master+OS/materialsA4.pdf?job=dist)
[![The Axis OOB](.imgs/ax-oobOS.png)](https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master+OS/materialsA4.pdf?job=dist)
[![The counters](.imgs/countersOS.png)](https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master+OS/materialsA4.pdf?job=dist)
[![VASSAL module](.imgs/vassalOS.png)](https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master+OS/AfrikaKorps.vmod?job=dist)

## Implementation 

The whole package is implemented in LaTeX using my package
[_wargame_](https://gitlab.com/wargames_tex/wargame_tex).  This package,
combined with the power of LaTeX, produces high-quality documents,
with vector graphics to ensure that everything scales.   Since the
board is quite big, we must use Lua$`\mathrm{\LaTeX}`$ for the
formatting. 

## The General articles 

[All _The General_
issues](https://www.vftt.co.uk/ah_mags.asp?ProdID=PDF_Gen) as scanned PDFs.

- First edition (1964)

  - "Can the British Really Win in Afrika Korps", [_The General_,
    **Vol.1**, #1,
    p1](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2001%20No%201.pdf#page=1)
  - "Contest # 1 - Afrika Korps", [_The General_, **Vol.1**, #1, p5,
    p1](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2001%20No%201.pdf#page=5)
  - "Afrika Korps - Sea Movement", [_The General_, **Vol.1**, #1, p11,
    p1](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2001%20No%201.pdf#page=11)
  - "You're not reading the rules", [_The General_, **Vol.1**, #2, p3,
    p1](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2001%20No%202.pdf#page=3)
  - "Afrika Korps Rules Supplement", [_The General_, **Vol.1**, #2,
    p3-41](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2001%20No%202.pdf#page=3)
  - Madeja, V., "Play Balance for Afrika Korps", [_The General_, **Vol.1**,
    #2,
    p7](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2001%20No%202.pdf#page=7)
  - Madeja, V., "Sink Some African Sands", [_The General_, **Vol.1**, #3,
    p3](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2001%20No%203.pdf#page=3)
  - Perica, J., "Absurdity of Afrika Korps", [_The General_, **Vol.1**, #3,
    p10-11](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2001%20No%203.pdf#page=10)
  - Knabe, C., "Afrika Korps - Tactics of Isolation", [_The General_,
    **Vol.1**, #4,
    p4](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2001%20No%204.pdf#page=4)
  
- Second edition, first printing (1965)

  - "Africa Korps - Replayed", [_The General_, **Vol.1**, #5,
    p2,12](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2001%20No%205.pdf#page=2)
  - "A Critique - Afrika Korps - Replayed", [_The General_, **Vol.1**, #6,
    p2](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2001%20No%206.pdf#page=2)
  - Nofi, A., "9 December 1949", [_The General_, **Vol.1**, #6,
    p7](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2001%20No%206.pdf#page=7)
  - "Play-by-Mail Table for Afrika Korps", [_The General_, **Vol.1**, #6,
    p7](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2001%20No%206.pdf#page=7)
  - Whiskeyman, D.B., "Tactics and Strategy", [_The General_, **Vol.2**,
    #1,
    p9](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2002%20No%201.pdf#page=9)
  - Whiskeyman, D.B., "Tactics and Strategy - Part 2", [_The General_,
    **Vol.2**, #2,
    p9](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2002%20No%202.pdf#page=9)
  - Finch, J., "Tactics on Afrika Korps", [_The General_, **Vol.2**, #2,
    p9](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2002%20No%202.pdf#page=9)
  - Johnson, J., "A New German Approach to Afrika Korps", [_The General_,
    **Vol.2**, #4,
    p6](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2002%20No%204.pdf#page=6)
  - Tribolet, J., "Basic German Strategy in Afrika Korps", [_The
    General, **Vol.2**, #6,
    p12](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2002%20No%206.pdf#page=12)
  - Russel, F., "Basic Allied Strategy in Afrika Korps", [_The General_,
    **Vol.3**, #1,
    p10](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2003%20No%201.pdf#page=10)
  - Epperson, J., "Rading Rommel", [_The General_, **Vol.3**, #1,
    p10](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2003%20No%201.pdf#page=10)
  - Wood, A., "Rommel Rides Again", [_The General_, **Vol.3**, #3,
    p9](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2003%20No%203.pdf#page=9)
  - Stone, B., "Across the Sands", [_The General_, **Vol.3**, #4,
    p9](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2003%20No%204.pdf#page=9)
  - Meagher, C., "Operation Oasis", [_The General_, **Vol.3**, #4,
    p9](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2003%20No%204.pdf#page=9)
  - Tribolet, J., "Major Battles and Campaigns - Libya and Egypt,
    1940-42", [_The General_, **Vol.3**, #6,
    p5-6](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2003%20No%206.pdf#page=5)
  - Cragoe, D.,  "Rommel's Thrust", [_The
    General, **Vol.4**, #1, p10-11](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2004%20No%201.pdf#page=10)
  - Rosenberg, M., "German Strategy in Afrika Korps", [_The General_,
    **Vol.4**, #2,
    p11](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2004%20No%202.pdf#page=11)
  - Duvall, B., "Afrika Korps and Attrition Table", [_The General_,
    **Vol.4**, #5,
    p4](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2004%20No%205.pdf#page=4)
  - Mathews, L.,  "Best Allied Defence - an Aggressive German", [_The
    General, **Vol.5**, #4, p10](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2005%20No%204.pdf#page=10)
  - Olson, R., "The Real Afrika Korps", [_The General_, **Vol.5**, #5,
    p4-5](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2005%20No%205.pdf#page=4)
  - Lee, D., "The Road to Alexandria", [_The General_, **Vol.5**, #6,
    p7](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2005%20No%206.pdf#page=7)
  - Searight, W., "Brais vs Brawn", [_The General_, **Vol.5**, #6,
    p7](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2005%20No%206.pdf#page=7)
  - Quinn, B., "All or Nothing? - Hardly", [_The General_, **Vol.5**, #6,
    p11](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2005%20No%206.pdf#page=11)
  - Searight, W., "D.A.K.", [_The General_, **Vol.7**, #4,
    p6](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2007%20No%204.pdf#page=6)
  - "Contest # 44", [_The General_, **Vol.8**, #2,
    p13](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2008%20No%202.pdf#page=13)
  - Hazlett, T., "Afrika Korps Thesis", [_The General_, **Vol.8**, #3,
    p6-7](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2008%20No%203.pdf#page=6)
  - Searight, W., "Tobruch?", [_The General_, **Vol.9**, #1,
    p10-11](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2009%20No%201.pdf#page=10)
  - Bagget, L., "A Southern Strategy", [_The General_, **Vol.9**, #4,
    p7-8](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2009%20No%204.pdf#page=7)
  - "Contest # 53", [_The
    General, **Vol.9**, #5, p13](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2009%20No%205.pdf#page=13)
  - Garbish, R., "Operation Crusader - The Winter Battle", [_The
    General, **Vol.10**, #4,
    p4-7](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2010%20No%204.pdf?page=4)
  - Garbish, R. &amp; Hoyer, B., "Series Replay - Afrika Koprs", [_The
    General, **Vol.10**, #5, p17-20](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2010%20No%205.pdf#page=17)
  - "Contest # 59", [_The General_, **Vol.10**, #5,
    p21](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2010%20No%205.pdf#page=21)
  - Hazlett, T., "Tournament Play", [_The General_, **Vol.11**, #3,
    p9-10](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2011%20No%203.pdf#page=9)
  - "Contest # 62", [_The General_, **Vol.11**, #3,
    p17](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2011%20No%203.pdf#page=17)
  - DeWitt, K., "A Decade with Das Afrika Korps", [_The General_,
    **Vol.11**, #5,
    p6-9](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2011%20No%205.pdf#page=6)
  
- Second edition, second printing (1975)

  - Hazlett, T., Libby, B., &amp; Burdick, D., "Series Replay - Afrika Krops", [_The
    General, **Vol.12**, #4, p20-24](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2012%20No%204.pdf#page=24)
  - Lockwood, J., "The Paleveda Gambit: A New British Gambit for Afrika
    Korps", [_The General_, **Vol.12**, #5,
    p14-16](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2012%20No%205.pdf#page=14)
  - Beyma, R.J, Burdick, D.S., &amp; Hazlett, T., "Afrika Korps -
    Replay", [_The General_, **Vol.13**, #5,
    p14-16,21-22](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2013%20No%205.pdf#page=14)
  - "Contest # 77", [_The General_, **Vol.14**, #1,
    p20](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2010%20No%205.pdf#page=20)
  - Roberts, D., "Competitional Afrika Korps - Replay", [_The General_,
    **Vol.14**, #1,
    p28-29](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2014%20No%201.pdf#page=28)
  - Packwood, S.S., "Another Afrika Korps Gambit", [_The General_,
    **Vol.15**, #5,
    p15-16](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2015%20No%205.pdf#page=15)
  - Preissle, F., "K.O. in Round Five ... Or Give up the Home Base?
    Never, never, never!", [_The General_, **Vol.16**, #2,
    p22-23](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2016%20No%202.pdf#page=22)

- Third edition (1980)

  - Lockwood, J., "Afrika Korps Theory - A Tournament Player's View of
    Afrika Korps", [_The General_, **Vol.17**, #3,
    p4-12](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2017%20No%203.pdf#page=4)
  - "Contest # 97", [_The General_, **Vol.17**, #3,
    p50](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2017%20No%203.pdf#page=50)
  - Preissle, F., "Face-lift for a Classic Beauty - The New-look Afrika
    Korps; 1980 Edition", [_The General_, **Vol.18**, #1,
    p27-30](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2018%20No%201.pdf#page=27)
  - Gutenkunst, R.J., "Desert Deception - Adding the True Role of
    Reconnaissance Afrika Korps", [_The General_, **Vol.18**, #2,
    p12-16](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2018%20No%202.pdf#page=12)
  - Preissle, F., "Operation Torchlight - The End-game in Afrika Korps",
    [_The General_, **Vol.18**, #3,
    p22-26](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2018%20No%203.pdf#page=22)
  - Beyma, R., "Back to Basics - Basic German Alternatives in Afrika
    Korps", [_The General_, **Vol.18**, #4,
    p42-43](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2018%20No%204.pdf#page=42)
  - Blumburg, A., "Operation Compass - A Variant for Afrika Korps",
    [_The General_, **Vol.22**, #1,
    p35-38](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2022%20No%201.pdf#page=35)
  - Gutenkunst, R., "Upgunning an Old Warrior - Afrika Korps for
    Everybody", [_The General_, **Vol.25**, #3,
    p21-22](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2025%20No%203.pdf#page=21)

Also

- [YouTube howto][] by [Legendary Tactics][]
- [YouTube history and playthrough][]


[artifacts.zip]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/download?job=dist

[AfrikaKorpsA4.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/AfrikaKorpsA4.pdf?job=dist
[AfrikaKorpsA4Booklet.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/AfrikaKorpsA4Booklet.pdf?job=dist
[materialsA4.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/materialsA4.pdf?job=dist
[splitboardA3.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/splitboardA3.pdf?job=dist
[splitboardA4.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/splitboardA4.pdf?job=dist
[hexes.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/hexes.pdf?job=dist
[AfrikaKorps.vmod]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/AfrikaKorps.vmod?job=dist


[AfrikaKorpsLetter.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-Letter-master/AfrikaKorpsLetter.pdf?job=dist
[AfrikaKorpsLetterBooklet.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-Letter-master/AfrikaKorpsLetterBooklet.pdf?job=dist
[materialsLetter.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-Letter-master/materialsLetter.pdf?job=dist
[splitboardTabloid.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-Letter-master/splitboardTabloid.pdf?job=dist
[splitboardLetter.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-Letter-master/splitboardLetter.pdf?job=dist




[AfrikaKorpsA4OS.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master+OS/AfrikaKorpsA4.pdf?job=dist
[AfrikaKorpsA4BookletOS.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master+OS/AfrikaKorpsA4Booklet.pdf?job=dist
[materialsA4OS.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master+OS/materialsA4.pdf?job=dist
[splitboardA3OS.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master+OS/splitboardA3.pdf?job=dist
[splitboardA4OS.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master+OS/splitboardA4.pdf?job=dist
[hexesOS.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master+OS/hexes.pdf?job=dist
[AfrikaKorpsOS.vmod]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master+OS/AfrikaKorps.vmod?job=dist


[AfrikaKorpsLetterOS.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-Letter-master+OS/AfrikaKorpsLetter.pdf?job=dist
[AfrikaKorpsLetterBookletOS.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-Letter-master+OS/AfrikaKorpsLetterBooklet.pdf?job=dist
[materialsLetterOS.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-Letter-master+OS/materialsLetter.pdf?job=dist
[splitboardTabloidOS.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-Letter-master+OS/splitboardTabloid.pdf?job=dist
[splitboardLetterOS.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-Letter-master+OS/splitboardLetter.pdf?job=dist

[YouTube howto]: https://youtu.be/uHOtUG4JvlQ
[Legendary Tactics]: https://www.youtube.com/@LegendaryTactics
[YouTube history and playthrough]: https://youtu.be/vl2OGkQIubk



