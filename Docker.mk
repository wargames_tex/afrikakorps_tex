# -*- Makefile -*-
#
# Rules for docker jobs
#
# Variables needed
#
# - CI_COMMIT_REF_NAME (optional)
#
# Targets needed
#
# - vmod
#
docker::
	docker run --user root --group-add users -e GRANT_SUDO=yes -it --rm \
		-v $(PWD):/root/$(notdir $(PWD)) texlive/texlive \
		/bin/bash

docker-apt::
	apt update
	apt install -y wget python3-pil poppler-utils

docker-wargame:: docker-apt
	wget "https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/download?job=dist" -O wargame.zip
	mkdir -p ${HOME}/texmf
	unzip -o wargame.zip -d ${HOME}/texmf

docker-prep::	docker-wargame

docker-build::docker-prep
	$(MAKE) docker-artifacts \
		VERSION=${CI_COMMIT_REF_NAME} \
		VERBOSE=1 \
		LATEX_FLAGS=

docker-artifacts:: distdirA4 distdirLetter vmod
#
# EOF
#
