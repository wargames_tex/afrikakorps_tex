#!/usr/bin/env python

from sys import path
path.append('../wargame_tex/utils')

from wgexport import VLogUpgrader 

class MyUpgrader(VLogUpgrader):
    def other(self,line,verbose):
        # from re import match
        # 
        # m = match(r'LOG\s+TURN\S+\s+(.*)',line)
        # if not m:
        #     return line
        # 
        # rest  = m.group(1)
        # parts = rest.split(';')
        # phase = int(parts[4].strip('\\'))
        # 
        # decl = str(not phase in [2,3,7,8]).lower()
        # reso = str(not phase in [3,8]).lower()
        # 
        # print('Declare',decl,'Resolve',reso)
        # 
        # line += '\n' + \
        #     f'LOG\tMutableProperty NotDeclare {decl} {decl} Module\\\\\n'+\
        #     f'LOG\tMutableProperty NotResolve {reso} {reso} Module\\\\\n'
        #         
        # 
        # print(line)

        return line
    
if __name__ == '__main__':
    from argparse import ArgumentParser

    ap = ArgumentParser(description='Try to fix')
    ap.add_argument('vmod',help='Vassal Module')
    ap.add_argument('vlog',help='Vassal Log')
    ap.add_argument('-v','--verbose',help='Be verbose',action='store_true')
    ap.add_argument('-o','--output',help='Output file name',type=str)

    args = ap.parse_args()

    vmodFileName = args.vmod
    vlogFileName = args.vlog
    outFileName  = args.output

    if not outFileName:
        from pathlib import Path
        
        inPath      = Path(logFileName)
        outPath     = inPath.with_suffix('.new')
        outFileName = str(outPath)

    upgrader = MyUpgrader(vmodFileName, vlogFileName)
    upgrader.upgrade()
    upgrader.write(outFileName)

#
# EOF
#
