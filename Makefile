#
#
#
NAME		:= AfrikaKorps
VERSION		:= 1.2
VMOD_VERSION	:= 2.1.1
VMOD_TITLE	:= Afrika Korps

LATEX		:= lualatex

DATAFILES	:= materials.tex		\
		   hexes.tex			\
		   tables.tex			\
		   oob.tex
DATA2FILES	:= board.tex			\
		   wargame.rough.tex
DOCFILES	:= $(NAME).tex			\
		   front.tex			\
		   rules.tex			\
		   notes.tex			\
		   $(DATAFILES)
STYFILES	:= ak.sty			\
		   commonwg.sty
BOARD		:= hexes.pdf
ROUGH		:= wargame.rough.pdf
SIGNATURE	:= 24
PAPER		:= --a4paper
TUTORIAL	:= Tutorial.vlog
IMGS		:= .imgs

TARGETSA4	:= $(NAME)A4.pdf 		\
		   $(NAME)A4Booklet.pdf 	\
		   splitboardA3.pdf		\
		   splitboardA4.pdf		\
		   materialsA4.pdf		\
		   hexes.pdf			\
		   $(NAME).vmod

TARGETSLETTER	:= $(NAME)Letter.pdf 		\
		   $(NAME)LetterBooklet.pdf 	\
		   splitboardTabloid.pdf	\
		   splitboardLetter.pdf		\
		   materialsLetter.pdf

SUBMAKE_FLAGS	:= --no-print-directory

ifdef VERBOSE	
MP4_FLAGS	:= -v
endif

include Variables.mk
include Patterns.mk

all:	$(TARGETSA4) $(NAME).vmod 
a4:	$(TARGETSA4)
us:	$(TARGETSLETTER)
vmod:	$(NAME).vmod
letter:	us
mine:	a4 cover.pdf box.pdf theoryA4Booklet.pdf

oldschool:
	$(MAKE) clean
	@echo "Will create ol'school variant"
	$(MUTE) touch .oldschool
	$(MAKE) $(filter-out oldschool, $(MAKECMDGOALS))
	rm -f .oldschool

$(IMGS)/%.png:%.png
	$(MUTE)$(CONVERT) $< $(CONVERT_FLAGS) $@

update-images:	$(IMGS)/counters.png	\
		$(IMGS)/front.png	\
		$(IMGS)/tables.png	\
		$(IMGS)/hexes.png	\
		$(IMGS)/al-oob.png	\
		$(IMGS)/ax-oob.png

update-images-os:
	mkdir -p $(IMGS)+OS
	$(MAKE) oldschool update-images IMGS:=$(IMGS)+OS
	$(MUTE)rm -f $(IMGS)+OS/logo.png
	$(MUTE)for i in $(IMGS)+OS/*.png ; do \
	  mv $$i $(IMGS)/`basename $$i .png`OS.png ; done
	$(MUTE)rm -rf $(IMGS)+OS

$(IMGS)/al-oob.png $(IMGS)/ax-oob.png:oobA4.pdf
	@echo "PDFTOCAIRO	$<"
	$(MUTE)$(PDFTOCAIRO) $(CAIROFLAGS) -png $<
	@echo "CONVERT		$<"
	$(MUTE)$(CONVERT) oobA4-1.png $(CONVERT_FLAGS) $(IMGS)/al-oob.png
	$(MUTE)$(CONVERT) oobA4-2.png $(CONVERT_FLAGS) $(IMGS)/ax-oob.png
	$(MUTE)rm -f oobA4-1.png oobA4-2.png 

include Rules.mk


clean::
	@echo "CLEAN"
	$(MUTE)rm -f .oldschool
	$(MUTE)$(MAKE) -C ak clean 

realclean:: clean
	@echo "CLEAN ALL"
	$(MUTE)rm -rf __pycache__ $(NAME)-*-$(VERSION)+OS*
	$(MUTE)$(MAKE) -C ak clean 

$(ROUGH):		wargame.rough.tex

$(NAME)A4.pdf:		$(NAME)A4.aux
$(NAME)A4.aux:		$(DOCFILES) 	$(STYFILES)     splitboardA4.pdf
materialsA4.pdf:	$(DATAFILES)	$(STYFILES)	splitboardA4.pdf
theoryA4.pdf:		theoryA4.aux	$(STYFILES)     $(BOARD)
theoryA4Booklet.pdf:	SIGNATURE=16

$(NAME)Letter.pdf:	$(NAME)Letter.aux
$(NAME)Letter.aux:	$(DOCFILES) 	$(STYFILES) 	splitboardLetter.pdf
$(NAME)LetterBooklet.pdf:PAPER=--letterpaper
materialsLetter.pdf:	$(DATAFILES)	$(STYFILES)	splitboardLetter.pdf
theoryLetter.pdf:	theoryLetter.aux $(STYFILES)    $(BOARD)
theoryLetterBooklet.pdf: SIGNATURE=16
theoryLetterBooklet.pdf: PAPER=--letterpaper

cover.pdf:		cover.tex $(DATAFILES) $(STYFILES) $(BOARD)
box.pdf:		box.tex   $(DATAFILES) $(STYFILES)
front.pdf:		front.tex $(DATAFILES) $(STYFILES) $(BOARD)
$(BOARD):		hexes.tex tables.tex $(ROUGH) $(STYFILES)

export.pdf:		export.tex tables.tex hexes.tex oob.tex $(STYFILES)

$(IMGS)/hexes.png:	CONVERT_FLAGS=$(RCONVERT_FLAGS)

distdirA4OS:
	@echo "DISTDIR	$(NAME)-A4-$(VERSION)+OS"
	$(MUTE)$(MAKE) oldschool a4 VMOD_TITLE:="$(VMOD_TITLE) - Ol'School"
	$(MUTE)$(MAKE) distdirA4 VERSION:=$(VERSION)+OS

distdirLetterOS:
	@echo "DISTDIR	$(NAME)-Letter-$(VERSION)+OS"
	$(MUTE)$(MAKE) oldschool a4 VMOD_TITLE:="$(VMOD_TITLE) - Ol'School"
	$(MUTE)$(MAKE) distdirLetter VERSION:=$(VERSION)+OS

include Docker.mk

docker-artifacts::distdirA4OS distdirLetterOS

.PRECIOUS:	$(NAME).vtmp $(NAME)Rules.pdf
#
# EOF
#

